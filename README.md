## lbdm - a Lattice Boltzmann by Dario Maggiolo

* oct/  Octave-based running codes
* src/  C-based code in progress
* etc/  Etcetera ..

#### Prerequisites

1. `openmpi` library available on openmpi-website _https://www.open-mpi.org/software/ompi/v2.0/_
2. `hdf5` library available on hdf5-group-website _https://support.hdfgroup.org/HDF5/release/obtainsrc518.html#conf_ (to be compiled in parallel)
3. `git` (avaliable with linux _apt-get_)
4. `octave` for tests (available with linux _apt-get_)

Instructions for installations are in the corresponding websites.

#### Basic instructions for start:

1. `git clone https://gitlab.com/dariom/lbdm` (gitlab username and password required ?)
2. `cd lbdm`
3. `git checkout -b _new-branch-name_`
4. `git branch`
5. `git merge origin master`
6. `git pull origin master`

For pushing on remote your branch ask to be aproject member to the admin, then:

7. `git remote set-url origin https://_your-username_/gitlab.com/dariom/lbdm.git`
8. `git push -u origin _new-branch-name_` (it will ask for gitlab password)
9. `git remote show origin`

Explanation will come ..

#### Run tests:

Tests are available under `src/test/###_NAMEOFTEST/` where you will find bash executables.

#### Instructions for running and make-type commands:

LBdm basics for run:
* edit your _user/user-defined.c_ file following the _user/user-defined.template.c_ template
* `make clean` && `make init` && `make` && `mpirun -np # ./lbdm`

Commands:
1. `make clean`: clean output make files
2. `make superclean': clean output make files and _dat_ or _h5_ files in _out_ directory
3. `make init`: necessary for feeding the code with  _user/user-defined.c_ where user defines sim variables
4. `make`: well, it makes
5. `make movesim`: after a simulation move all the dumped files to a directory called with as the _NAME_ variable of the simulation

#### Instructions for updating the code (in order to prevent messy situations, follow this instructions carefully):

Passage 2 will be unecessary if you didn't touch the code as Developer and you are working as Visitor in your local master branch.

1. `make clean`, for cleaning the build-up code
2. `git status -uno` and check if:
	* your branch is ahead of your origin, if so, give `git push`,
	* your branch is behind of your origin, if so, give `git pull`,
	* there are _files_ mentioned as _Changes not staged for committ_, if so, check (e.g. with `git diff`) wheter they are **good intentional changes** or **bad unintentional changes**. If they are good give `git add _files_` else `git checkout _files_`. Now your directory should be clean and you can give `git commit -m "_your-message_"`, and finally `git push`
3. `git merge origin master`
4. `git pull origin master`:
5. `git status -uno` and check if:
	* your branch is up-to-date with the remote one, if so, you are already updated with the latest version of the code,
	* your branch is ahead of remote one by some commits, if so give `git push`

#### Instructions for making your changes avaliable on master branch, i.e. for making them worldwide:

1. `git checkout -b merge-`_your-branch-name_
2. `git push -u origin merge`-_your-branch-name_
3. Go to `https://gitlab.com/dariom/lbdm` and create a new merge request from the source branch `merge-`_your-branch-name_ to the target branch `develop`. If and only if all the planned tests will pass, the merge in _master_ branch will be considered by the admin.

#### Instructions for geometry input file:

Please refer to _src/in/HELPGEO_


If you have any problem send an email to _maggiolo@chalmers.se_
