#ifndef MACRO_H_
#define MACRO_H_

int macro_nse();
int macro_nse_zero();
int macro_twophase();
int periodic_macro();
int periodic_int();
int macro_forcing();
int macro_nse_inc();
int macro_nse_clean();
int macro_nse_dump();
int macro_nse_chk();
int macro_nse_inout();

#endif
