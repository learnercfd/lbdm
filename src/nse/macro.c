#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include "hdf5.h"
#include "macro.h"
#include "../var/shared.h"

#ifdef THERMAL
int macro_nse (dfdq *f, macro *ux, macro *uy, macro *uz, macro *rho, dfdq *t, macro *tem) {
#else
int macro_nse (dfdq *f, macro *ux, macro *uy, macro *uz, macro *rho) {
#endif
    
  int idx=0, i=0, j=0, k=0, q=0;

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

        for (q=0; q<NQ; q++) {
       
        ux[idx] += f[idx].dq[q] * cx[q]; 	
        uy[idx] += f[idx].dq[q] * cy[q]; 	
        uz[idx] += f[idx].dq[q] * cz[q]; 	
        rho[idx] += f[idx].dq[q];

        #ifdef THERMAL
        tem[idx] += t[idx].dq[q];
        #endif

        }
      }
    }
  }
}

#ifdef THERMAL
int macro_nse_zero (macro *ux, macro *uy, macro *uz, macro *rho, macro *tem) {
#else
int macro_nse_zero (macro *ux, macro *uy, macro *uz, macro *rho) {
#endif
    
  int idx=0, i=0, j=0, k=0, q=0;

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

      ux[idx]=0.0;
      uy[idx]=0.0;
      uz[idx]=0.0;
      rho[idx]=0.0;

      #ifdef THERMAL
      tem[idx]=0.0;
      #endif

      }
    }
  }
}

int periodic_macro (double *rho) {

  MPI_Status status1;

  /* int MPI_Sendrecv(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
     int dest, int sendtag, void *recvbuf, int recvcount,
     MPI_Datatype recvtype, int source, int recvtag,
     MPI_Comm comm, MPI_Status *status) */


  MPI_Sendrecv(rho+lzp*lxp*ly+lzp+1    , 1, mpi_maplane_y, pryp, 3, \
               rho+lzp+1               , 1, mpi_maplane_y, prym, 3, mpi_comm_y, &status1);
  MPI_Sendrecv(rho+lzp*lxp+lzp+1       , 1, mpi_maplane_y, prym, 4, \
               rho+lzp*lxp*(ly+1)+lzp+1, 1, mpi_maplane_y, pryp, 4, mpi_comm_y, &status1);

  MPI_Sendrecv(rho+lzp*lx+1    , 1, mpi_maplane_x, prxp, 1, \
               rho+1           , 1, mpi_maplane_x, prxm, 1, mpi_comm_x, &status1);
  MPI_Sendrecv(rho+lzp+1       , 1, mpi_maplane_x, prxm, 2, \
               rho+lzp*(lx+1)+1, 1, mpi_maplane_x, prxp, 2, mpi_comm_x, &status1);

  MPI_Sendrecv(rho+lz  , 1, mpi_maplane_z, przp, 5, \
               rho     , 1, mpi_maplane_z, przm, 5, mpi_comm_z, &status1);
  MPI_Sendrecv(rho+1   , 1, mpi_maplane_z, przm, 6, \
               rho+lz+1, 1, mpi_maplane_z, przp, 6, mpi_comm_z, &status1);

}

int periodic_int (int *lsol) {

  MPI_Status status1;

  /* int MPI_Sendrecv(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
     int dest, int sendtag, void *recvbuf, int recvcount,
     MPI_Datatype recvtype, int source, int recvtag,
     MPI_Comm comm, MPI_Status *status) */

  MPI_Sendrecv(lsol+lzp*lxp*ly+lzp+1    , 1, mpi_intplane_y, pryp, 3, \
               lsol+lzp+1               , 1, mpi_intplane_y, prym, 3, mpi_comm_y, &status1);
  MPI_Sendrecv(lsol+lzp*lxp+lzp+1       , 1, mpi_intplane_y, prym, 4, \
               lsol+lzp*lxp*(ly+1)+lzp+1, 1, mpi_intplane_y, pryp, 4, mpi_comm_y, &status1);

  MPI_Sendrecv(lsol+lzp*lx+1    , 1, mpi_intplane_x, prxp, 1, \
               lsol+1           , 1, mpi_intplane_x, prxm, 1, mpi_comm_x, &status1);
  MPI_Sendrecv(lsol+lzp+1       , 1, mpi_intplane_x, prxm, 2, \
               lsol+lzp*(lx+1)+1, 1, mpi_intplane_x, prxp, 2, mpi_comm_x, &status1);

  MPI_Sendrecv(lsol+lz  , 1, mpi_intplane_z, przp, 5, \
               lsol     , 1, mpi_intplane_z, przm, 5, mpi_comm_z, &status1);
  MPI_Sendrecv(lsol+1   , 1, mpi_intplane_z, przm, 6, \
               lsol+lz+1, 1, mpi_intplane_z, przp, 6, mpi_comm_z, &status1);

}

#ifdef TWOPHASE
int macro_forcing (int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *psix, macro *psiy, macro *psiz) {
#else
int macro_forcing (int *lsol, macro *ux, macro *uy, macro *uz, macro *rho) {
#endif

  int idx=0, i=0, j=0, k=0;

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

        if (lsol[idx] !=0) {

        #ifdef GRAVITY
        ux[idx] += 0.5 * rho[idx] *(double) DPDL * (double) dpdlv[0];
        uy[idx] += 0.5 * rho[idx] *(double) DPDL * (double) dpdlv[1];
        uz[idx] += 0.5 * rho[idx] *(double) DPDL * (double) dpdlv[2];

        #else
        ux[idx] += 0.5 * (double) DPDL * (double) dpdlv[0];
        uy[idx] += 0.5 * (double) DPDL * (double) dpdlv[1];
        uz[idx] += 0.5 * (double) DPDL * (double) dpdlv[2];
        #endif  

        #ifdef TWOPHASE
        ux[idx] += (double)TAU * (double) psix[idx];
        uy[idx] += (double)TAU * (double) psiy[idx];
        uz[idx] += (double)TAU * (double) psiz[idx];
        #endif

        }
      }
    }
  }
}

int macro_nse_inc (int *lsol, macro *ux, macro *uy, macro *uz, macro *rho) {

  int i=0, j=0, k=0, idx=0;
  
  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

      rho[idx] = (double)(rho[idx]*lsol[idx]);
      ux[idx]  = (double)ux[idx]/((double)rho[idx]+(double)(1-lsol[idx]));
      uy[idx]  = (double)uy[idx]/((double)rho[idx]+(double)(1-lsol[idx]));
      uz[idx]  = (double)uz[idx]/((double)rho[idx]+(double)(1-lsol[idx]));

      }
    }
  }
}

int macro_nse_clean (int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *psix, macro *psiy, macro *psiz, double C) {

  int i=0, j=0, k=0, idx=0;
  
  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

      // Purging from Van der Waals Forces
      ux[idx] += C * (double) (psix[idx]/(rho[idx]+(double)(1-lsol[idx])));
      uy[idx] += C * (double) (psiy[idx]/(rho[idx]+(double)(1-lsol[idx])));
      uz[idx] += C * (double) (psiz[idx]/(rho[idx]+(double)(1-lsol[idx])));

      }
    }
  }
}

int macro_twophase (int *lsol, macro *rho, macro *psix, macro *psiy, macro *psiz) {

  /* Psudopotential forcing will lead to EOS of the kind:
  pressure P = rho (T|cs2) + G cs2 /2 psi**2  */

  int i=0, j=0, k=0, idx=0;
  int q=0, idxq=0, idxiq=0;
  int nq=0;
 
  #ifdef NEIGHBOURING
  int countne=0;
  int inq=0, jnq=0, knq=0;
  int iniq=0, jniq=0, kniq=0;
  double psine=0.0;
  #endif

  //Temporary allocate PSI
  macro *PSI;
  PSI=(macro *)calloc(lxp*lyp*lzp, sizeof(macro)); 

  for (j=0; j<lyp; j++) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idx = IDXP(i,j,k);

        // Set fictious density at walls
        if (lsol[idx] == 0) {

        #ifdef NEIGHBOURING
        rho[idx] = 0.0; // will be overwritten
        #else
        rho[idx] = (double)RHOWALL;
        #endif

        }

      // Determine PSI Pseudopotential and zero xyz force
      #ifdef THERMAL
      PSI[idx] = (double) exp(-1./(double)rho[idx]);
      #else
      PSI[idx] = sqrt(RHOZ) * (double)( 1.-exp( -(double)rho[idx]/(double)RHOZ) );
      #endif

      psix[idx] = 0.0;
      psiy[idx] = 0.0;
      psiz[idx] = 0.0;

      }
    }
  }

  // Running over available neighbours
  // once we set psi for wall/fluid nodes.
  #ifdef NEIGHBOURING

  for (j=0; j<lyp; j++) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idx = IDXP(i,j,k);

        if (lsol[idx] == 0) { 
        countne=0;
        psine=0.0;

          //Pair of opposites directions
          for (nq=0; nq<(int)(NQ-1)/2; nq++) {
          q=(int)vq[nq];

          inq = i+cx[q];
          jnq = j+cy[q];
          knq = k+cz[q];

          iniq = i+cx[iq[q]];
          jniq = j+cy[iq[q]];
          kniq = k+cz[iq[q]];

            // Check if it's inside domain [q] 
            if ((inq>=0&&inq<lxp)&&(jnq>=0&&jnq<lyp)&&(knq>=0&&knq<lzp)) {

            idxq = IDXP(inq,jnq,knq);
  
              // And if it's fluid [q]
              if (lsol[idxq] == 1) {
              countne++;
              psine += PSI[idxq];

              }
            }

            // Check if it's inside domain [iq]
            if ((iniq>=0&&iniq<lxp)&&(jniq>=0&&jniq<lyp)&&(kniq>=0&&kniq<lzp)) {

            idxiq = IDXP(iniq,jniq,kniq);
  
              // And if it's fluid [iq]
              if (lsol[idxiq] == 1) {
              countne++;
              psine += PSI[idxiq];

              }
            }
          }

          if (countne>0) {
          PSI[idx] = (double)(psine/(double)countne) + (double)RHOWALL;

          }
        }
      }
    }
  }

  #endif

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);

        for (nq=0; nq<(int)(NQ-1)/2; nq++) { 
        q=(int)vq[nq];

        idxq = IDXP(i+cx[q],j+cy[q],k+cz[q]);
        idxiq = IDXP(i+cx[iq[q]],j+cy[iq[q]],k+cz[iq[q]]);

	//xyz Forcing on the basis of PSI(rho)
        psix[idx] += PSI[idx] * ((double)GG * (double)w[q] * PSI[idxq] * (double)cx[q] + \
                                 (double)GG * (double)w[iq[q]] * PSI[idxiq] * (double)cx[iq[q]]);
        psiy[idx] += PSI[idx] * ((double)GG * (double)w[q] * PSI[idxq] * (double)cy[q] + \
                                 (double)GG * (double)w[iq[q]] * PSI[idxiq] * (double)cy[iq[q]]);
        psiz[idx] += PSI[idx] * ((double)GG * (double)w[q] * PSI[idxq] * (double)cz[q] + \
                                 (double)GG * (double)w[iq[q]] * PSI[idxiq] * (double)cz[iq[q]]);

        }
      }
    }
  }

  free(PSI);

} 

int macro_nse_inout (int iter, int *lsol, macro *rho) {

  int q=0, i=0, j=0, k=0, idx=0, idx1=0;

  // Inlet
  #if (defined DENSITY_XM || defined DENSITY_YM || defined DENSITY_ZM || \
       defined VELOCITY_XM || defined VELOCTITY_YM || defined VELOCITY_ZM)

  #if (defined DENSITY_XM || defined VELOCITY_XM)
  //Check if processor is involved
  if ((int)prx == (int)0) {

    i = 0;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_YM || defined VELOCITY_YM)
  //Check if processor is involved
  if ((int)pry == (int)0) {

    j = 0;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_ZM || defined VELOCITY_ZM)
  //Check if processor is involved
  if ((int)prz == (int)0) {

    k = 0;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

  #endif

      idx = IDXP(i,j,k);

      #if (defined DENSITY_XM || defined VELOCITY_XM)
      idx1 = IDXP(i+1,j,k);
      #elif (defined DENSITY_YM || defined VELOCITY_YM)
      idx1 = IDXP(i,j+1,k);
      #elif (defined DENSITY_ZM || defined VELOCITY_ZM)
      idx1 = IDXP(i,j,k+1);
      #endif

      rho[idx] = rho[idx1];

      // Correction required at first step if BCs not symmetric
      if (iter == 0)
      lsol[idx] = lsol[idx1];

      }
    }
  }

  #endif

  // Outlet
  #if (defined DENSITY_XP || defined DENSITY_YP || defined DENSITY_ZP || \
       defined VELOCITY_XP || defined VELOCTITY_YP || defined VELOCITY_ZP)

  #if (defined DENSITY_XP || defined VELOCITY_XP)
  //Check if processor is involved
  if ((int)prx == (int)NEX-1) {

    i = lx+1;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_YP || defined VELOCITY_YP)
  //Check if processor is involved
  if ((int)pry == (int)NEY-1) {

    j = ly+1;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_ZP || defined VELOCITY_ZP)
  //Check if processor is involved
  if ((int)prz == (int)NEZ-1) {

    k = lz+1;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

  #endif

      idx = IDXP(i,j,k);

      #if (defined DENSITY_XP || defined VELOCITY_XP)
      idx1 = IDXP(i-1,j,k);
      #elif (defined DENSITY_YP || defined VELOCITY_YP)
      idx1 = IDXP(i,j-1,k);
      #elif (defined DENSITY_ZP || defined VELOCITY_ZP)
      idx1 = IDXP(i,j,k-1);
      #endif

      rho[idx] = rho[idx1];

      // Correction required at first step if BCs not symmetric
      if (iter == 0)
      lsol[idx] = lsol[idx1];

      }
    }
  }

  #endif

}

int macro_nse_chk (int iter, int *lsol, macro *rho, macro *ux, macro *uy, macro *uz) {

  int itchk = 0;
  double residual = 0.0;
  double re = 0.0, re_all=0.0;
  int i=0, j=0, k=0, idx=0;
  int sizep = 0;
  char fname[128];
  FILE *fileconv;

  if (ROOT(world_rank)) {
    sprintf(fname,"out/conv_re.dat");
    fileconv = fopen(fname,"a");
  }

  // Single procs
  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);
 
        if (lsol[idx] != 0) {
        sizep += (int)lsol[idx];
        #if (defined DPDLX && !defined DPDLY && !defined DPDLZ)
        re += (double)(ux[idx]*(double)CLENGTH/(CS2*(TAU-0.5))); 
        #elif (defined DPDLY && !defined DPDLX && !defined DPDLZ)
        re += (double)(uy[idx]*(double)CLENGTH/(CS2*(TAU-0.5))); 
        #elif (defined DPDLZ && !defined DPDLY && !defined DPDLX)
        re += (double)(uz[idx]*(double)CLENGTH/(CS2*(TAU-0.5))); 
        #else
        re += (double)(sqrt(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)) * \
              (double)CLENGTH/(CS2*(TAU-0.5)));
        #endif
        }
      }
    }
  }

  // All procs
  re = (double) re/sizep; 
  MPI_Allreduce (&re, &re_all, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  re_all = (double)(re_all/NP);
  residual = (double)(sqrt(pow((re_all-retmp)/retmp,2.)));
  retmp = re_all;

  if (ROOT(world_rank)) {
    if (iter % OUT_ITER == 0) {
    fprintf(stderr,"II: CHK at iter %d Re = %g\n",iter,re_all);
    fprintf(stderr,"II: CHK at iter %d Residual = %g\n",iter,residual);
    }

  fprintf(fileconv,"%g %g\n",re_all,residual);
  fclose(fileconv);

  // Check
  itchk = (int)(iter/CHK_ITER);
  RECONV[itchk-1] = residual;

    if (isnan(re_all)) {

    fprintf(stderr,"EE: Reynolds NaN Error.\n");
    exit(1); 

    }

    if (residual <= RESIDUAL) {

    // Eventually modify for better convergence
    #ifdef TWOPHASE
    if ((int)iter>50) {
      if ((int)ITERCONVTP+CHK_ITER==(int)iter) { 
      CONVFLAGTP++;
        if ((int)CONVFLAGTP>2) {
        fprintf(stderr,"\nII: Converged! Yuppi!\n\n");
        CONVFLAG=1;
        }
      }
    ITERCONVTP = (int)iter;
    }
    #else

    fprintf(stderr,"\nII: Converged! Yuppi!\n\n");
    CONVFLAG=1;

    #endif

    }
  }

  MPI_Bcast(&CONVFLAG, 1, MPI_INT, 0, MPI_COMM_WORLD);

}

#ifdef THERMAL
int macro_nse_dump (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, macro *tem) {
#else
int macro_nse_dump (int iter, int *lsol, macro *ux, macro *uy, macro *uz, macro *rho) {
#endif

  if (ROOT(world_rank))
  fprintf(stderr,"II: DUMPING macroscopic quantities Navier-Stokes at iter %d\n",iter); 

  int idx=0, i=0, j=0, k=0;
  FILE *fileout;
  char filename[128];

  #ifdef DUMPASCII

  sprintf(filename,"out/dump.%s.%05d.p_%d.dat",NAME,iter,world_rank);
  fileout = fopen(filename,"w");

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx = IDXP(i,j,k);
      fprintf(fileout,"%g %g %g %g %d\n",ux[idx],uy[idx],uz[idx],rho[idx],lsol[idx]);

      }
    }
  }

  fclose(fileout);

  #endif

  
  #ifdef DUMPHDF5

  hid_t file_id;
  hid_t group_id; 
  hid_t hdf5_status;
  hid_t dataset_id, dataset_id_2, dataset_id_3, dataset_id_4, dataset_id_5;
  hid_t plist_id, efilespace, edimespace;
  hid_t ret, string_type, xfer_plist;
  hsize_t efile_3d[3], edime_3d[3];
  hsize_t estart_3d[3], ecount_3d[3], estride_3d[3], eblock_3d[3];
  hsize_t dstart_3d[3], dcount_3d[3], dstride_3d[3], dblock_3d[3];
  herr_t status;

  H5E_auto_t old_func;
  void *old_client_data;

  H5Eget_auto (H5E_DEFAULT, &old_func, &old_client_data);
  H5Eset_auto (H5E_DEFAULT, old_func, old_client_data);

  sprintf (filename, "out/dump.%s.%05d.h5",NAME,iter);

  // Dump RHO

  efile_3d[0] = NY;
  efile_3d[1] = NX;
  efile_3d[2] = NZ;

  edime_3d[0] = lyp;
  edime_3d[1] = lxp;
  edime_3d[2] = lzp;

  estart_3d[0] = 1;
  estart_3d[1] = 1;
  estart_3d[2] = 1;
  estride_3d[0] = 1;
  estride_3d[1] = 1;
  estride_3d[2] = 1;
  eblock_3d[0] = ly;
  eblock_3d[1] = lx;
  eblock_3d[2] = lz;
  ecount_3d[0] = 1;
  ecount_3d[1] = 1;
  ecount_3d[2] = 1;

  dstart_3d[0] = procoffy[world_rank];
  dstart_3d[1] = procoffx[world_rank];
  dstart_3d[2] = procoffz[world_rank];
  dstride_3d[0] = 1;
  dstride_3d[1] = 1;
  dstride_3d[2] = 1;
  dblock_3d[0] = ly;
  dblock_3d[1] = lx;
  dblock_3d[2] = lz;
  dcount_3d[0] = 1;
  dcount_3d[1] = 1;
  dcount_3d[2] = 1;


  plist_id = H5Pcreate (H5P_FILE_ACCESS);
  hdf5_status = H5Pset_fapl_mpio (plist_id, MPI_COMM_WORLD, MPI_INFO_NULL); 

  file_id = H5Fcreate (filename, H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
  group_id = H5Gcreate (file_id, "/LBdm", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id = H5Dcreate (group_id, "rho", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  status = H5Dwrite (dataset_id, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, rho);

  H5Dclose (dataset_id);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Dump UX

  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_2 = H5Dcreate (group_id, "ux", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  status = H5Dwrite (dataset_id_2, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, ux);

  H5Dclose (dataset_id_2);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Dump UY

  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_3 = H5Dcreate (group_id, "uy", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  status = H5Dwrite (dataset_id_3, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, uy);

  H5Dclose (dataset_id_3);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Dump UZ

  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_4 = H5Dcreate (group_id, "uz", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  status = H5Dwrite (dataset_id_4, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, uz);

  H5Dclose (dataset_id_4);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  // Dump SOL

  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_5 = H5Dcreate (group_id, "sol", H5T_NATIVE_INT, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  status = H5Dwrite (dataset_id_5, H5T_NATIVE_INT, edimespace, efilespace, xfer_plist, lsol);

  H5Dclose (dataset_id_5);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

// Dump TEMP
  #ifdef THERMAL

  hid_t dataset_id_6;

  efilespace = H5Screate_simple (3, efile_3d, NULL);
  edimespace = H5Screate_simple (3, edime_3d, NULL);

  dataset_id_6 = H5Dcreate (group_id, "tem", H5T_NATIVE_DOUBLE, efilespace, H5P_DEFAULT,H5P_DEFAULT, H5P_DEFAULT);

  status = H5Sselect_hyperslab (edimespace, H5S_SELECT_SET, estart_3d, estride_3d, ecount_3d, eblock_3d);
  status = H5Sselect_hyperslab (efilespace, H5S_SELECT_SET, dstart_3d, dstride_3d, dcount_3d, dblock_3d);

  xfer_plist = H5Pcreate (H5P_DATASET_XFER);
  ret = H5Pset_dxpl_mpio (xfer_plist, H5FD_MPIO_INDEPENDENT);

  status = H5Dwrite (dataset_id_6, H5T_NATIVE_DOUBLE, edimespace, efilespace, xfer_plist, tem);

  H5Dclose (dataset_id_6);
  H5Sclose (efilespace);
  H5Sclose (edimespace);
  H5Pclose (xfer_plist);

  #endif


  // Closing end

  H5Gclose (group_id);
  H5Fclose (file_id);
  H5Pclose (plist_id);


  #endif

  MPI_Barrier(MPI_COMM_WORLD);

}
