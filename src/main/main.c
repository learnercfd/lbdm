#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <mpi.h>
#include <unistd.h>
#include "../mpi/mpi_open.h"
#include "../var/shared.h"
#include "../in/geometry.h"
#include "../nse/macro.h"
#include "../lbe/micro.h"

int main(void)
{

// Set memory limit
setmemlimit();

// Allocate offsets of procs
procoffx = (int *) calloc (NP, sizeof(int));
procoffy = (int *) calloc (NP, sizeof(int));
procoffz = (int *) calloc (NP, sizeof(int));

// Init open-MPI
mpi_init(procoffz,procoffx,procoffy);

// Get world rank and size
MPI_Comm_size(MPI_COMM_WORLD, &world_size);
MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

// Three-dimensional
dpdlv = (double *) calloc (3, sizeof(double));

// Convergence array allocation
RECONV = (double *) calloc ((int)MAX_ITER/CHK_ITER, sizeof(double));

// Allocate weights, discrete speeds and MRT
w = (double *) calloc (NQ, sizeof(double)); 
cx = (int *) calloc (NQ, sizeof(int)); 
cy = (int *) calloc (NQ, sizeof(int)); 
cz = (int *) calloc (NQ, sizeof(int)); 
iq = (int *) calloc (NQ, sizeof(int)); 
vq = (int *) calloc ((NQ-1)/2, sizeof(int)); 

// Allocate free-slip q-speed (uptonow I allocate anyway)
qsx = (int *) calloc (10, sizeof(int)); 
qsy = (int *) calloc (10, sizeof(int)); 
qsz = (int *) calloc (10, sizeof(int)); 

// Allocate MRT only for D3Q19
#ifdef LBMRT
S = (D19 *) calloc (NQ, sizeof(D19));
iMRT = (D19 *) calloc (NQ, sizeof(D19));
MRT = (I19 *) calloc (NQ, sizeof(I19));
EQ = (D10 *) calloc (NQ, sizeof(D10));
EYE = (I19 *) calloc (NQ, sizeof(I19));
#endif

// Something to init?  
#ifdef LBMRT
var_init(w,cx,cy,cz,iq,vq,qsx,qsy,qsz,dpdlv,MRT,iMRT,EYE,S,EQ);
chk_mrt_matrix(MRT,iMRT,EYE,S,EQ);
#else
var_init(w,cx,cy,cz,iq,vq,qsx,qsy,qsz,dpdlv);
#endif

// Init message?
if (ROOT(world_rank))
fprintf(stderr,"II: LBdm on start: please refer to webpage https://gitlab.com/dariom/lbdm \n");

// Fluid Init?
if (ROOT(world_rank))
fprintf(stderr,"II: LB fluid parameters: fl.relaxation tau(U) = %g, initial rho0 = %g\n",TAU,RHO0);

// Two-phase to init?
#ifdef TWOPHASE
if (ROOT(world_rank))
fprintf(stderr,"II: Shan-Chen parameters: G = %g, coeff.rho0 = %g, wall density = %g\n",GG,RHOZ,RHOWALL);
#endif

// Thermal to init?
#ifdef THERMAL
if (ROOT(world_rank)) { 
fprintf(stderr,"II: Thermal parameters: th.relaxation tau(T) = %g, initial temp = %g\n",TAUT,TINIT);
fprintf(stderr,"II: Thermal parameters: wall temp = %g, subcooled temp = %g\n",TWALL,TSUB); }
#endif

// Check definitions
print_defs();

// Check processors number
if ((int)world_size!=(int)NP) {
fprintf(stderr,"EE: Error, number of procs mismatch!\n .. in fact world_size is %d while NP is %d\n",world_size,NP);
exit(1);
}

// Print procs geometry
if (ROOT(world_rank)) {
fprintf(stderr,"II: Global | NX:%d NY:%d NZ:%d\n",NX,NY,NZ);
fprintf(stderr,"II:  Local | lx:%d ly:%d lz:%d\n",lx,ly,lz);
}

// Print procs geometry
fprintf(stderr,"II: Proc num: %d | prx: %d pry: %d prz: %d\n",world_rank,prx,pry,prz);
fprintf(stderr,"II: Proc num: %d | prxm: %d prym: %d przm: %d\n",world_rank,prxm,prym,przm);
fprintf(stderr,"II: Proc num: %d | prxp: %d pryp: %d przp: %d\n",world_rank,prxp,pryp,przp);

// Print Name
FILE *fprname=NULL;
fprname = fopen("sim.name","w");
fprintf(fprname,"%s",NAME);
fclose(fprname);

// DECLARE
dfdq *f;
dfdq *feq;
#ifdef THERMAL
dfdq *t;
dfdq *teq;
#endif
dfdq *bf;
macro *ux;
macro *uy;
macro *uz;
macro *rho;
#ifdef TWOPHASE
macro *psix;
macro *psiy;
macro *psiz;
#endif
#ifdef THERMAL
macro *tem;
#endif
int *gsol;
int *lsol;

// ALLOCATE main micro-vars
f = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
feq = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
bf = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));

#ifdef THERMAL
// ALLOCATE main micro-vars for Temperature
t = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
teq = (dfdq *) calloc ((lxp)*(lyp)*(lzp),sizeof(dfdq));
#endif

// ALLOCATE main macro-vars
ux = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
uy = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
uz = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
rho = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));

#ifdef TWOPHASE
// ALLOCATE macro two-phase vars
psix = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
psiy = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
psiz = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
#endif

#ifdef THERMAL 
// ALLOCATE macro thermal vars
tem = (macro *) calloc ((lxp)*(lyp)*(lzp),sizeof(macro));
#endif

// ALLOCATE geometry GlobalSolid and LocalSolid
gsol = (int *) calloc (NX*NY*NZ,sizeof(int));
lsol = (int *) calloc (lxp*lyp*lzp,sizeof(int));

// Init Global Geometry
geometry_from_file(gsol);

// Divide Geometry in Local MPI Geometry
geometry_mpi(gsol, lsol);
free(gsol);

#ifdef THERMAL
// Init df and temperature df
micro_lbe_init(lsol,f,t);

#else
// Init df
micro_lbe_init(lsol,f);
#endif

// Periodic Bounday Conditions dfdq
periodic_df(f);

// Periodic Bounday Conditions lsol
periodic_int(lsol);

// Freeslip correction of lsol
geometry_slip(lsol);


/******************************/
/****        LOOOOP        ****/
/******************************/

if (ROOT(world_rank))
fprintf(stderr,"\nII: Inside LB time-LOOOOP .... %s\n\n",NAME);

  for (iter=0; iter<MAX_ITER+1; iter++) {

  #ifdef THERMAL
  // Navier-Stokes vars and Temperature at zero
  macro_nse_zero(ux,uy,uz,rho,tem);

  #else
  // Navier-Stokes vars at zero
  macro_nse_zero(ux,uy,uz,rho);
  #endif
  
  #ifdef THERMAL
  // Navier-Stokes macroscopic vars and Temperature
  macro_nse(f,ux,uy,uz,rho,t,tem);

  #else
  // Navier-Stokes macroscopic vars
  macro_nse(f,ux,uy,uz,rho);
  #endif

  #ifdef TWOPHASE
  // Periodic rho for two-phase
  periodic_macro(rho);

  // Periodic correction for Inout BCs 
  macro_nse_inout(iter,lsol,rho);

  // Pseudopoential for two-phase
  macro_twophase(lsol,rho,psix,psiy,psiz);
  #endif
  
  #ifdef TWOPHASE
  // Add forcing macroscopic (1) for two-phase
  macro_forcing(lsol,ux,uy,uz,rho,psix,psiy,psiz);

  #else
  // Add forcing macroscopic (1)
  macro_forcing(lsol,ux,uy,uz,rho);
  #endif
  
  // Navier-Stokes compressible correction
  macro_nse_inc(lsol,ux,uy,uz,rho);
  
  // Check Convergence
  if ((iter % CHK_ITER == 0) && ( iter != 0 ))
  macro_nse_chk(iter,lsol,rho,ux,uy,uz);

  #ifdef THERMAL 
  // Lattice-Boltzmann equilibrium distribution with Temperature
  micro_lbe_eq(lsol,teq,feq,rho,ux,uy,uz,tem,psix,psiy,psiz);
  
  #else
  // Lattice-Boltzmann equilibrium distribution
  micro_lbe_eq(lsol,feq,rho,ux,uy,uz);
  #endif

  #if (defined TWOPHASE && !defined THERMAL)
  // Navier-Stokes two-phase hydrodynamic velocities
  macro_nse_clean(lsol,ux,uy,uz,rho,psix,psiy,psiz,-(double)TAU+0.5);
  #endif

  #ifdef THERMAL
  // Dump Navier-Stokes quantities and Temperature
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,lsol,ux,uy,uz,rho,tem);

  #else
  // Dump Navier-Stokes quantities
  if ( iter % OUT_ITER == 0 )
  macro_nse_dump(iter,lsol,ux,uy,uz,rho);
  #endif

  #ifdef THERMAL
  // Dump again if converged (flag iter+1) with Temperature
  if ( CONVFLAG == 1 ) {
  MPI_Barrier(MPI_COMM_WORLD);
  macro_nse_dump(iter+1,lsol,ux,uy,uz,rho,tem);
  break; }

  #else
  // Dump again if converged (flag iter+1)
  if ( CONVFLAG == 1 ) {
  MPI_Barrier(MPI_COMM_WORLD);
  macro_nse_dump(iter+1,lsol,ux,uy,uz,rho);
  break; }
  #endif

  #ifdef TWOPHASE 
  // Navier-Stokes two-phase back to slip velocity
  macro_nse_clean(lsol,ux,uy,uz,rho,psix,psiy,psiz,+(double)TAU-0.5);
  #endif

  // Add forcing microscopic (2)
  micro_forcing(lsol,ux,uy,uz,rho,bf);
  
  #ifdef THERMAL
  // COLLISION Temperature
  micro_lbe_collision(lsol,t,teq,bf,MRT);
  #endif

  // COLLISION
  micro_lbe_collision(lsol,f,feq,bf,MRT);

  #ifdef THERMAL
  // Periodic Temperature
  periodic_df(t);
  #endif

  // Periodic
  periodic_df(f);

  // Free-slip (uptonow I called it anyway)
  micro_lbe_slip(lsol,f);

  #ifdef THERMAL
  // STREAMING Temperature
  micro_lbe_streaming(lsol,t,teq,1);
  #endif
  
  // STREAMING
  micro_lbe_streaming(lsol,f,feq,0);

  #ifdef THERMAL
  // Inlet/Outlet boundary conditions with Temperature
  micro_lbe_inout(lsol,f,t,rho,ux,uy,uz,tem);

  #else
  // Inlet/Outlet boundary conditions
  micro_lbe_inout(lsol,f,rho,ux,uy,uz);
  #endif

  } // End of LOOOOP

// Checks
//micro_lbe_chk(bf,lsol,"df"); 

// Finalize open-MPI
mpi_finalize();

}
