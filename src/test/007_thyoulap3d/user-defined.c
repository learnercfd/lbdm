/* ! ATTENTION do not delete or add strange rows !
Just either comment or uncomment the "Capabilities" 
and replace values of "Macros" with the desired ones */

/* ******************* */
/*  Capabilities here  */
/* ******************* */

//#define DPDLX

#define DUMPHDF5
//#define DUMPASCII

//#define LBMRT
#define TWOPHASE
#define THERMAL

//#define FREESLIP_XP
//#define FREESLIP_XM
//#define FREESLIP_YP
//#define FREESLIP_YM
//#define FREESLIP_ZP
//#define FREESLIP_ZM

//#define GRAVITY
//#define NEIGHBOURING

/* ******************* */
/*  Macros for folks   */
/* ******************* */

// Name identifiers
#define NAME "THYL3D1"
#define GEOM "yl3d.1.bin.txt"

// N-PROC
#define NP 2

// N-SIZE
#define NX 40
#define NY 40
#define NZ 40

// N-PROC PER EDGE
#define NEX 2
#define NEY 1
#define NEZ 1

// INIT DENSITY & dP/dL
#define RHO0 0.20
#define DPDL 0.00

// Iteration max,chk,out
#define MAX_ITER 800
#define CHK_ITER 1
#define OUT_ITER 200

// Q-speeds and t-relax
#define NQ 19
#define TAU 1.0

// RESIDUAL convergence
#define RESIDUAL 0.0001
#define CLENGTH 10

// TWOPHASE parameters
#define GG 9.5
#define RHO2 6.1
#define RHOZ 1.0
#define RHOWALL 0.0

// THERMAL parameters
#define TAUT 1.0    	
#define TINIT 0.2
#define TWALL 0.2
#define TSUB  0.2
