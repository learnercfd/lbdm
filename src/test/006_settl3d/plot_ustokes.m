XY=load('settl.xy.dat');
MM=load('settl.mm.dat');
UU=load('settl.uu.dat');

graphics_toolkit ("gnuplot");
setenv ("GNUTERM", "qt");

nit=MM(4);

figure, plot(XY(2:end,1),XY(2:end,2)-XY(2,2),'o');
hold on, plot(XY(end-round(nit*3/2):end,1),XY(end-round(nit*3/2):end,1).*MM(1)+MM(2),'-','color','m')

saveas(gcf,'plot_dx.png');

figure, plot(XY(:,1),UU(:,1),'o');
hold on, plot(XY(:,1),UU(:,2),'o','color','g');
hold on, plot(XY([end-nit,end],1),[MM(3),MM(3)],'-','color','r');

saveas(gcf,'plot_uu.png');
