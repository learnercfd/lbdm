L1=load('latent.1.dat');
A1=load('terms.1.dat');
T1=load('thickness.1.dat');

L2=load('latent.2.dat');
A2=load('terms.2.dat');
T2=load('thickness.2.dat');

graphics_toolkit ("gnuplot");
setenv ("GNUTERM", "qt");

figure,
hold on, plot(A1(:,1),A1(:,2),'o');
hold on, plot([A1(1,1),A1(end,1)],[A1(1,1),A1(end,1)]./L1(1)-L1(2)/L1(1),'-');
hold on, plot(A2(:,1),A2(:,2),'s');
hold on, plot([A2(1,1),A2(end,1)],[A2(1,1),A2(end,1)]./L2(1)-L2(2)/L2(1),'-');
saveas(gcf,'latent.png');

close,

figure, 
hold on, plot(T1(:,1),T1(:,2),'-');
hold on, plot(T1(:,1),T1(:,4),'o');
hold on, plot(T2(:,1),T2(:,2),'-');
hold on, plot(T2(:,1),T2(:,4),'s');
saveas(gcf,'thickness.png');


