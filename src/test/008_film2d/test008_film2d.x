#!/bin/bash -e

echo " "
echo "#lbdm# Test 008 Film Condensation 2d"
echo " "

git checkout user-defined.c

if ls terms.*.dat 1> /dev/null 2>&1; then
rm terms.*.dat 
fi

if ls latent.*.dat 1> /dev/null 2>&1; then
rm latent.*.dat 
fi

if ls thickness.*.dat 1> /dev/null 2>&1; then
rm thickness.*.dat 
fi

if ls dump.CONV.*.h5 1> /dev/null 2>&1; then
rm dump.CONV.*.h5
fi

if [ -f "thickness.png" ]; then
rm thickness.png 
fi

if [ -f "latent.png" ]; then
rm latent.png 
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

octave create_bin-txt.m

cp film2d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/008_film2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave heatlb.m

lambda=`awk 'NR==1 {printf ($1) }' latent.dat`
echo ""
echo "(Constant) Latent Heat / Specific Heat Capacity (1): " $lambda
echo ""

mv latent.dat latent.1.dat
mv terms.dat terms.1.dat
mv thickness.dat thickness.1.dat
mv dump.CONV.h5 dump.CONV.1.h5

############

sed -i -e 's/define DPDL 0.00006/define DPDL 0.00007/g' user-defined.c
sed -i -e 's/DPL=0.00006/DPL=0.00007/g' heatlb.m

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/008_film2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave heatlb.m

lambda=`awk 'NR==1 {printf ($1) }' latent.dat`
echo ""
echo "(Constant) Latent Heat / Specific Heat Capacity (2): " $lambda
echo ""

mv latent.dat latent.2.dat
mv terms.dat terms.2.dat
mv thickness.dat thickness.2.dat
mv dump.CONV.h5 dump.CONV.2.h5

############

octave plot_film.m

awk 'NR>=6&&NR<=29 {print (1-$4/$2)}' thickness.1.dat > error.dat
awk 'NR>=6&&NR<=29 {print (1-$4/$2)}' thickness.2.dat >> error.dat
awk '{if (($1*$1)^0.5<=0.1) print "ok"; else print "no" }' error.dat > errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/film2d.bin.txt

# clean local test
cd test/008_film2d/
git checkout user-defined.c
git checkout heatlb.m
rm *.txt

########### 1) Check NaN 

for file in terms.dat thickness.dat latent.dat error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 48 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
