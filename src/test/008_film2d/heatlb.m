load('dump.CONV.h5');

sol=double(LBdm.sol);
rho=LBdm.rho;
tem=LBdm.tem;
ux=LBdm.ux;

sol=sol(:,:,1);
rho=rho(:,:,1);
tem=tem(:,:,1);
ux=ux(:,:,1);

%%%%

TWALL=0.2;
NX=80;
NZ=30;
z0=2;

nu=1/6;
rho0=0.8;
DPL=0.00006;
cp=1.0;
alpha=1/3*0.07;

THR=5;

printf('\n');
printf('The set-up is made for simulating vapor-water thermodyanmic systems with non-dimensional numbers:\n');
printf('* Re(gas) ~ 10 \n* Ca(liq) ~ 10-3 \n* Pr(liq) ~ 0.14 \n* Ja(liq) ~ 10-2\n* Characteristic Length ~ 30 \n');
printf('\n');
printf('From Octave: \n');
printf('some checks ... \n');

th_rhol=6.1,
th_rhog=0.2,

%chk densities
lb_rhol=sum(rho(:).*((rho(:)>THR).*sol(:)),1)./sum(rho(:)>THR.*sol(:),1),
lb_rhog=sum(rho(:).*((rho(:)<=THR).*sol(:)),1)./sum(rho(:)<=THR.*sol(:),1),

th_vel=DPL*((NZ-2)**2)/12/(nu*rho0),

%chk velocity
lb_vel=sum(ux(:).*sol(:),1)./sum(sol(:),1),

x0=0;
count=0;
thereisfilm=0;

for (x=1:NX);
 for (z=z0:NZ-1);
 zlb=NZ-z+1;
 zlb_m=zlb+1;
 zlb_p=zlb-1;

  if (rho(zlb,x) >= THR);
  thereisfilm=1;
  end;

  if (z==NZ-1);
  thereisfilm=0;
  end;

  if (thereisfilm==1);
   if (rho(zlb,x) < THR);
   delta_thick = (THR-rho(zlb_m,x))/(rho(zlb,x)-rho(zlb_m,x));
   thick = (z-1)-z0+1 + delta_thick;
 
    if (thick > 0.5);
     if (count==0);
     x0=x-0.5;
     end;
    count=count+1;
    delta_tisat = delta_thick * (tem(zlb,x)-tem(zlb_m,x));
    Tisat = tem(zlb_m,x) + delta_tisat;
    Rex = lb_vel*(x-x0)/nu;
    taust = Rex**(3/2) * nu**2 *lb_rhog /(x-x0);
 
    X(count)=x-x0;
    TISAT(count)=Tisat;
    THICK(count)=thick;
    TUAST(count)=taust;
    end;
 
   break;
   end;
  end;

 end;
end;

%%% fit 
interval=6:36;

kappal = lb_rhol * (cp) * alpha;

a = DPL/(4*nu);
b = TUAST./(3*nu); 

axis_y = a.*THICK.**4 + b.*THICK.**3;
axis_z = a.*THICK.**4;
axis_x = kappal.*(TISAT-TWALL).*X;

[lambda othershit] = polyfit(axis_y(interval),axis_x(interval),1);
[lambdaz othershit] = polyfit(axis_z(interval),axis_x(interval),1);
LAMBDA=lambda(1);
LAMBDAZ=lambdaz(1);

latent_heat = LAMBDAZ;

e = - kappal/LAMBDA.*X.*(TISAT-TWALL);

D0 = 12.*a.*e;
D1 = 27.*b.**2.*e;
Q  = ((D1+sqrt(D1.**2-4.*D0.**3))./2).**(1/3);
p  = -3.*b.**2./(8.*a.**2);
q  = b.**3./(8.*a.**3);
S  = 0.5.*sqrt(-(2/3).*p+1./(3.*a).*(Q+D0./Q));

TH_AS = -b./(4*a)-S+0.5.*sqrt(-4.*S.**2-2.*p+q./S);
TH_NU = (kappal.*(TISAT-TWALL).*X.*4.*nu./(LAMBDAZ.*DPL)).**(1/4);


%%%% print
O=[LAMBDAZ,lambdaz(2),LAMBDA,lambda(2)];
save('latent.dat','-ascii','O');
N=[axis_x',axis_z',axis_y'];
save('terms.dat','-ascii','N');
M=[X',TH_NU',TH_AS',THICK'];
save('thickness.dat','-ascii','M');

