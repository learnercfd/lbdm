/* ! ATTENTION do not delete or add strange rows !
Just either comment or uncomment the "Capabilities" 
and replace values of "Macros" with the desired ones */

/* ******************* */
/*  Capabilities here  */
/* ******************* */

#define DPDLX

#define DUMPHDF5
//#define DUMPASCII

//#define LBMRT
#define TWOPHASE
#define THERMAL

//#define FREESLIP_XP
//#define FREESLIP_XM
//#define FREESLIP_YP
//#define FREESLIP_YM
//#define FREESLIP_ZP
//#define FREESLIP_ZM

//#define GRAVITY
#define NEIGHBOURING

/* ******************* */
/*  Macros for folks   */
/* ******************* */

// Name identifiers
#define NAME "FILM2D"
#define GEOM "film2d.bin.txt"

// N-PROC
#define NP 2

// N-SIZE
#define NX 120
#define NY 2
#define NZ 30

// N-PROC PER EDGE
#define NEX 2
#define NEY 1
#define NEZ 1

// INIT DENSITY & dP/dL
#define RHO0 0.8
#define DPDL 0.00006

// Iteration max,chk,out
#define MAX_ITER 10000
#define CHK_ITER 1
#define OUT_ITER 400

// Q-speeds and t-relax
#define NQ 19
#define TAU 1.0

// RESIDUAL convergence
#define RESIDUAL 0.000001
#define CLENGTH 28

// TWOPHASE parameters
#define GG 9.5
#define RHO2 6.1
#define RHOZ 1.0
#define RHOWALL 0.0

// THERMAL parameters
#define TAUT 0.57    	
#define TINIT 0.37
#define TWALL 0.37
#define TSUB  0.2
