rand('seed',3);
printf('oct-Seed: %d',rand('seed'));

NS=30;
R=2;

[xx zz yy] = meshgrid(1:40,1:16,1:8);
xr = rand(NS,1).*(20)+10.5;
zr = rand(NS,1).*16+0.5;
yr = rand(NS,1).*8+0.5;

A = (sqrt((xx-20.5).^2+(zz-8.5).^2) <= R*2);

A=A+1;
A(A==2)=0;

U=A(:);
save('cyl2d.bin.txt','-ascii','U');

A=zeros(16,40,8);
B=ones(16,40,8);

for i=1:NS;
A = (sqrt((xx-xr(i)).^2+(yy-yr(i)).^2+(zz-zr(i)).^2) <= R);
B(A==1)=0;
end;

U=B(:);
save('rnd3d.bin.txt','-ascii','U');

C=zeros(size(B,1)+2,size(B,2),size(B,3)+2);
C(1,:,2:end-1)=B(1,:,:);
C(end,:,2:end-1)=B(end,:,:);
C(2:end-1,:,1)=B(:,:,1);
C(2:end-1,:,end)=B(:,:,end);
C(2:end-1,:,2:end-1)=B;

U=C(:);
save('rnd3d.free.bin.txt','-ascii','U');
