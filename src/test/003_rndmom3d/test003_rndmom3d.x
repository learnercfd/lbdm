#!/bin/bash -e

echo " "
echo "#lbdm# Test 003 Random Beads Momentum Check 3d"
echo " "

git checkout user-defined.c

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

octave create_bin-txt.m
cp cyl2d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/003_rndmom3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave momentum_h5.m

###########

cp rnd3d.bin.txt ../../in/

## RANDOM BED 
sed -i -e 's/define NAME .*/define NAME "RNDBED3D"/g' user-defined.c
sed -i -e 's/define GEOM .*/define GEOM "rnd3d.bin.txt"/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make 

if (mpirun -np 2 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/003_rndmom3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)

cp $nameconv dump.CONV.h5

octave momentum_h5.m

###########

## We ll check MRT 
sed -i -e 's/\/\/#define LBMRT/#define LBMRT/g' user-defined.c
sed -i -e 's/define RESIDUAL .*/define RESIDUAL 0.0000001/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (3) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/003_rndmom3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave momentum_h5.m

###########

awk 'NR>=1 && NR<=40 {if (($2*$2)^0.5<=0.0002) print "ok"; else print "no" }' error.dat >> errchk.dat
awk 'NR>=41 && NR<=80 {if (($2*$2)^0.5<=0.003) print "ok"; else print "no" }' error.dat >> errchk.dat
awk 'NR>=81 && NR<=120 {if (($2*$2)^0.5<=0.0003) print "ok"; else print "no" }' error.dat >> errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/cyl2d.bin.txt
rm in/rnd3d.bin.txt

# clean local test
cd test/003_rndmom3d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 120 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
