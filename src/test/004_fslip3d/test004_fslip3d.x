#!/bin/bash -e

echo " "
echo "#lbdm# Test 004 Free Slip Test 2d and 3d"
echo " "

git checkout user-defined.c

if [ -f "zickhomsy_fitted.dat" ]; then
rm zickhomsy_fitted.dat 
fi

if [ -f "proofval.png" ]; then
rm proofval.png 
fi

if [ -f "drag_lbdm.dat" ]; then
rm drag_lbdm.dat
fi

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error_2d.dat" ]; then
rm error_2d.dat 
fi

if [ -f "error_3d.dat" ]; then
rm error_3d.dat 
fi

if [ -f "error_drag.dat" ]; then
rm error_drag.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

if [ -f "ux_profile_lbdm_2d.dat" ]; then
rm ux_profile_lbdm_2d.dat
fi

if [ -f "ux_profile_lbdm_3d.dat" ]; then
rm ux_profile_lbdm_3d.dat
fi

octave create_bin-txt.m
cp fs2d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/004_fslip3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave ux_h5_fslip2d.m

paste ux_profile_2d.dat ux_profile_lbdm_2d.dat | awk 'NR<=18 {print ($1/$2-1)}' > error_2d.dat 

###########

cp fs3d.bin.txt ../../in/

sed -i -e 's/\/\/#define FREESLIP_YP/#define FREESLIP_YP/g' user-defined.c

sed -i -e 's/define NAME .*/define NAME "FREESLIP3D"/g' user-defined.c
sed -i -e 's/define GEOM .*/define GEOM "fs3d.bin.txt"/g' user-defined.c

sed -i -e 's/define NP .*/define NP 4/g' user-defined.c

sed -i -e 's/define NX .*/define NX 4/g' user-defined.c
sed -i -e 's/define NY .*/define NY 20/g' user-defined.c
sed -i -e 's/define NZ .*/define NZ 20/g' user-defined.c

sed -i -e 's/define NEX .*/define NEX 1/g' user-defined.c
sed -i -e 's/define NEY .*/define NEY 2/g' user-defined.c
sed -i -e 's/define NEZ .*/define NEZ 2/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 4 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/004_fslip3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave ux_h5_fslip3d.m

paste ux_profile_3d.dat ux_profile_lbdm_3d.dat | awk 'NR<=13 {print ($1/$2-1)}' > error_3d.dat 

###########

cp fs3d.sp.bin.txt ../../in/

sed -i -e 's/\/\/#define LBMRT/#define LBMRT/g' user-defined.c
sed -i -e 's/\/\/#define FREESLIP_YM/#define FREESLIP_YM/g' user-defined.c
sed -i -e 's/\/\/#define FREESLIP_ZP/#define FREESLIP_ZP/g' user-defined.c

sed -i -e 's/define NAME .*/define NAME "FREESPHERE3D"/g' user-defined.c
sed -i -e 's/define GEOM .*/define GEOM "fs3d.sp.bin.txt"/g' user-defined.c

sed -i -e 's/define NP .*/define NP 4/g' user-defined.c

sed -i -e 's/define NX .*/define NX 16/g' user-defined.c
sed -i -e 's/define NY .*/define NY 18/g' user-defined.c
sed -i -e 's/define NZ .*/define NZ 18/g' user-defined.c

sed -i -e 's/define NEX .*/define NEX 1/g' user-defined.c
sed -i -e 's/define NEY .*/define NEY 2/g' user-defined.c
sed -i -e 's/define NEZ .*/define NEZ 2/g' user-defined.c

sed -i -e 's/define DPDL .*/define DPDL 0.000078125/g' user-defined.c

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 4 ./lbdm); then
echo "Run (3) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/004_fslip3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave ux_h5_fslip3d_sphere.m
octave fit_zickhomsy.m

paste drag_lbdm.dat zickhomsy_fitted.dat | awk 'NR<=1 {print ($2/$4-1)}' >> error_drag.dat 

###########

awk 'NR<=18 {if (($1*$1)^0.5<=0.008) print "ok"; else print "no" }' error_2d.dat > errchk.dat
awk 'NR<=11 {if (($1*$1)^0.5<=0.08) print "ok"; else print "no" }' error_3d.dat >> errchk.dat
awk 'NR==1 {if (($1*$1)^0.5<=0.02) print "ok"; else print "no" }' error_drag.dat >> errchk.dat

echo "------ "
echo "errors:"
cat error_drag.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/fs2d.bin.txt
rm in/fs3d.bin.txt
rm in/fs3d.sp.bin.txt

# clean local test
cd test/004_fslip3d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in drag_lbdm.dat zickhomsy_fitted.dat error_2d.dat error_3d.dat error_drag.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 30 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
