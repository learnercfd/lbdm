A=ones(20,4,2);
A(end,:,:)=0;
U=A(:);
save('fs2d.bin.txt','-ascii','U');

[xx zz yy] = meshgrid(1:4,1:20,1:20);
B = (sqrt((zz-1.5).**2+(yy-19.5).**2) <= 18);
B(1,:,end)=0;
U=B(:);
save('fs3d.bin.txt','-ascii','U');

[xx zz yy] = meshgrid(1:16,1:18,1:18);
A = (sqrt((xx-8.5).^2+(yy-9.5).^2+(zz-1.5).^2) > 4) & (sqrt((xx-8.5).^2+(yy-9.5).^2+(zz-17.5).^2) > 4);

A(1,:,1)=0;
A(end,:,end)=0;
A(1,:,end)=0;
A(end,:,1)=0;

U=A(:);

save('fs3d.sp.bin.txt','-ascii','U');
