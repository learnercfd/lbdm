load('dump.CONV.h5');

dpl=0.000078125;
rs=4;
mu=1/6;

sol=double(LBdm.sol);
ux=LBdm.ux;
solfs=sol(2:end-1,:,2:end-1);
uxfs=ux(2:end-1,:,2:end-1);
e=mean(solfs(:));
U=mean(uxfs(:).*solfs(:))./e;
K=U*e*dpl**(-1)*mu;
drag=2*rs**2/9/K*1/(1-e);

M=[(1-e),drag];
save('drag_lbdm.dat','-ascii','M');
