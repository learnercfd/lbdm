int=10;
top=810;

x1=4;
x2=12;

it=0;
for i=0:int:top;
it=it+1;

name=sprintf('dump.TRANS2D.%05d.h5',i);
load(name);
ux=LBdm.ux;
rho=LBdm.rho;
sol=double(LBdm.sol);
QMx=sum(sum(rho.*ux.*sol,3),1);
figure(2), hold on, plot(QMx,'color',[i/top 0 0])

RHt(it)=sum(sum(sum(rho(:,x1+1:x2,:),3),1),2)/sum(sum(sum(sol(:,x1+1:x2,:),3),1),2);
Vol=sum(sum(sum(sol(:,x1+1:x2,:),3),1),2);

if ((it>1)&&(it<=(top/int)));
dQMx(it)=(QMx(x2)-QMx(x1));
end;

if (it>2);
dRHt(it-1)=(RHt(it)-RHt(it-2))./2/int;
end; 


end;

M=(1-dQMx(2:end)./(-Vol.*dRHt(2:end)))';
save('error.dat','-ascii','M');

figure(1),
hold on, plot(dQMx(2:end),'o');
hold on, plot(-Vol.*dRHt(2:end),'.');
saveas(gcf,'continuity.png','png');
