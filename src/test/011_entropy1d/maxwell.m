dt=load('DT.dat');

rg=dt(:,1);
rl=dt(:,2);
tg=dt(:,3);
tl=dt(:,4);

dr=0.01;
G1=9.5;
G2=4.0;

toler=10**-3;

for i=1:12;

if (i<=6);
G=G1;
end;
if (i>6);
G=G2;
end;

p0(i)=rg(i).*tg(i)-G/6.*exp(-2./rg(i));
vr=[rg(i):dr:rl(i)];

A(i)=0; B(i)=0;
for j=1:size(vr,2);
sig=(p0(i)-tg(i).*vr(j)+G/6.*exp(-2./vr(j)))./vr(j).**2;

if (sig<toler);
A(i)=(((p0(i)-tg(i).*vr(j)+G/6.*exp(-2./vr(j)))./vr(j).**2).*dr)+A(i);
jx=j;
end;

if (sig>toler);
B(i)=(((p0(i)-tg(i).*vr(j)+G/6.*exp(-2./vr(j)))./vr(j).**2).*dr)+B(i);
end; 

end;

if (i==1||i==7);
figure(1),
hold on, plot(1./vr,tg(i).*vr-G/6.*exp(-2./vr),'-');
hold on, plot([1/vr(end),1/vr(1)],[p0(i),p0(i)],'--');
end;
title('EOS and Maxwell Area');

err(i)=(A(i)+B(i))/(p0(i).*(vr(1)-vr(end)));

end;

M=err';
save('error.dat','-ascii','M');

saveas(gcf,'maxwell.png','png');

