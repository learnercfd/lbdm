z1=11;
z2=31;

cv=3/2;

dt=load('DT.dat');

rg=dt(:,1);
rl=dt(:,2);
tg=dt(:,3);
tl=dt(:,4);

for i=1:12
figure(1), 
hold on, plot(tg(i),rg(i),'o');
hold on, plot(tl(i),rl(i),'o');
end;
title('equilibrium densities');
saveas(gcf,'eqdens.png','png');

T=0.5.*(tg+tl);

sg1=-log(rg(1:6)./tg(1:6).**(cv));
sl1=-log(rl(1:6)./tl(1:6).**(cv));
ds1=sl1(1:5)-sg1(2:6);

sg2=-log(rg(7:12)./tg(7:12).**(cv));
sl2=-log(rl(7:12)./tl(7:12).**(cv));
ds2=sl2(1:5)-sg2(2:6);

latent_heat(1)=-mean(T(2:6).*ds1);
latent_heat(2)=-mean(T(8:12).*ds2);
save('latentheat.dat','-ascii','latent_heat');

figure(2),
hold on, plot(T(2:6),-T(2:6).*ds1,'o');
hold on, plot(T(8:12),-T(8:12).*ds2,'o');
xlim([0.13,0.24]);
ylim([0,1]);
title('latent heat');
saveas(gcf,'entropic.png','png');
