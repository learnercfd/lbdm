#!/bin/bash -e

echo " "
echo "#lbdm# Test 009 Velocity and Pressure BCs 2d"
echo " "

git checkout user-defined.c

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

if [ -f "ux_profile_lbdm.dat" ]; then
rm ux_profile_lbdm.dat
fi

if [ -f "ux_profile.dat" ]; then
rm ux_profile.dat
fi

if [ -f "ux_h5_po2d.m" ]; then
rm ux_h5_po2d.m
fi

octave create_bin-txt.m
cp bcs2d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/009_bcs2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

cp ../000_po2d/ux_h5_po2d.m .
cp ../000_po2d/ux_profile.dat .

octave ux_h5_po2d.m

paste ux_profile.dat ux_profile_lbdm.dat | awk 'NR<=18 {print ($1/$2-1)}' > error.dat 
awk 'NR<=18 {if (($1*$1)^0.5<=0.036) print "ok"; else print "no" }' error.dat > errchk.dat

###########

## We ll check FASTER viscosity independent sims tau=1.5 
sed -i -e 's/\/\/#define VELOCITY_XM/#define VELOCITY_XM/g' user-defined.c
sed -i -e 's/#define DENSITY_XM/\/\/#define DENSITY_XM/g' user-defined.c

cp user-defined.c ../../user/
cd ../..

make superclean
make init
make 

if (mpirun -np 2 ./lbdm); then
echo "Run (2) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/009_bcs2d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave ux_h5_po2d.m

paste ux_profile.dat ux_profile_lbdm.dat | awk 'NR<=18 {print ($1/$2-1)}' >> error.dat 
awk 'NR>=20&&NR<=35 {if (($1*$1)^0.5<=0.1) print "ok"; else print "no" }' error.dat >> errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/bcs2d.bin.txt

# clean local test
cd test/009_bcs2d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in ux_profile.dat error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 34 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
