tau=1.0;

dpl=0.000078125;
mu=1/3*(tau-0.5)*1.0;

rs=4;
load('dump.CONV.h5');

uy=LBdm.uy;
sol=LBdm.sol;
e=mean(sol(:))
U=mean(uy(:))./e
K=U*e*mu*dpl**(-1)
drag=2*rs**2/9/K*1/(1-e)

M=[1-e,drag];
save('drag_lbdm.dat','-ascii','-append','M');
