R=[19];

[xx yy zz]=meshgrid(1:80,1:40,1:40);
S=(sqrt((yy-20.5).**2+(zz-20.5).**2)<R);
A=int32(S);
A(:,1:15,:)=1;
A(:,16:40,:)=2;
U=A(:);
name=sprintf('wash3d.bin.txt',i);
save(name,'-ascii','U');
