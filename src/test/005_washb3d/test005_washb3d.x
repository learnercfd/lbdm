#!/bin/bash -e

echo " "
echo "#lbdm# Test 005 Washburn Imbibition 3d"
echo " "

git checkout user-defined.c

if [ -f "plot_sat.png" ]; then
rm plot_sat.png 
fi

if [ -f "plot_wash.png" ]; then
rm plot_wash.png 
fi

if [ -f "sat_th.dat" ]; then
rm sat_th.dat
fi

if [ -f "sat_lb.dat" ]; then
rm sat_lb.dat
fi

if [ -f "*.bin.txt" ]; then
rm *.bin.txt
fi

if [ -f "dump.CONV.h5" ]; then
rm dump.CONV.h5
fi

if [ -f "error.dat" ]; then
rm error.dat 
fi

if [ -f "errchk.dat" ]; then
rm errchk.dat
fi

if [ -f "PASSED" ]; then
rm PASSED
fi

octave create_bin-txt.m
cp wash3d.bin.txt ../../in/

cp user-defined.c ../../user/
cd ../..
make superclean
make init
make

if (mpirun -np 2 ./lbdm); then
echo "Run (1) successfull.."
else
echo "ERROR: occurring in mpirun."
exit 1
fi

cd test/005_washb3d/

nameconv=$(ls ../../out/dump.* | sort -V | tail -n 1)
cp $nameconv dump.CONV.h5

octave sat_h5.m
octave plot_sat.m

paste sat_lb.dat sat_th.dat | awk 'NR>=2&&NR<=21 {print ($1/$2-1)}' > error.dat 

###########

awk 'NR>=12&&NR<=20 {if (($1*$1)^0.5<=3.5) print "ok"; else print "no" }' error.dat > errchk.dat

echo "------ "
echo "errors:"
cat error.dat
echo "-------"

###########

# clean source
cd ../..
make clean
rm in/wash3d.bin.txt

# clean local test
cd test/005_washb3d/
git checkout user-defined.c
rm *.txt

########### 1) Check NaN 

for file in sat_lb.dat sat_th.dat error.dat errchk.dat
do
if grep -iq "NaN\|Inf" $file
then
echo "ERROR NaN!"
exit 1
fi
done

########### 2) Check Residual

if [ -f "errchk.dat" ]; then

numchk=`wc -l < errchk.dat`
echo $numchk

  if [ $numchk -eq 9 ]
  then

    if grep -Fxq 'no' errchk.dat
    then

    echo "ERROR greater than expected !!"
    exit 1

    else

    touch "PASSED"
    echo "TEST PASSED!"
    exit 0

    fi

  else

  echo "ERROR errchk.dat has not the expected number of rows !!"
  exit 1

  fi

else

echo "ERROR errchk.dat does not exist ??"
exit 1

fi
