lb=load('sat_lb.dat');

graphics_toolkit ("gnuplot");
setenv ("GNUTERM", "qt");

xx=lb(:,1);
yy=0.5.*xx.*0.5;

figure, plot(xx,lb(:,2),'o')
hold on, plot(xx,yy,'-')
set(gca,'xscale','log','yscale','log')

saveas(gcf,'plot_sat.png','png');

close;

load('dump.CONV.h5');
rho=LBdm.rho;
figure, contourf(rho(:,:,20));
pbaspect([2 1])
hold on, plot([70,70-20*cos(pi()/180*62)],[0,20*sin(pi()/180*62)],'-','linewidth',2,'color','white')
saveas(gcf,'plot_wash.png')

