close all
clear all

itmax=1000;
itout=50;

ang=62;
thr=1.0;

rhog=0.12;
rhol=2.4;
rhow=1.4;

nu=1/6;

LX=80;
LZ=40;
LY=40;
LW=40;
RW=19;

gamma=0.1;
%ang=(rhow-rhol)/(rhog-rhol)*180,
ugamma=gamma*cos(pi()/180*ang)*2*RW/(nu*rhol*LW);

for i=1:itmax/itout+1;

name=sprintf('../../out/dump.WASH3D.%05d.h5',(i-1)*itout);
load(name);

rho=LBdm.rho;
sol=double(LBdm.sol);
sat(i)=sum(sum(sum(sol(:,end-LW+1:end,:).*(rho(:,end-LW+1:end,:)>thr),3),1),2)/sum(sum(sum(sol(:,end-LW+1:end,:),3),1),2);
tph(i)=(i-1)*itout*ugamma/LW;

end;

err=abs(1-sat(end-2)./(0.5.*tph(end-2).**0.5)),

M=[tph',sat'];
save('sat_lb.dat','-ascii','M');

yy=0.5.*tph.**0.5;
N=[tph',yy'];
save('sat_th.dat','-ascii','N');

