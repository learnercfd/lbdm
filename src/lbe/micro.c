#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <stddef.h>
#include "micro.h"
#include "../var/shared.h"

#ifdef THERMAL
int micro_lbe_init(int *lsol, dfdq *f, dfdq *t) {
#else
int micro_lbe_init(int *lsol, dfdq *f) {
#endif

  if (ROOT(world_rank))
  fprintf(stderr,"II: Init microscopic quantities Lattice-Boltzmann\n");

  int idx=0, i=0, j=0, k=0, q=0, var=0;

  for (j=0; j<lyp; j++) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idx = IDXP(i,j,k);

        if (lsol[idx] == 1) {

          for (q=0; q<NQ; q++) {
 
          #if (defined DENSITY_XM && defined DENSITY_XP && !defined TWOPHASE)
          f[idx].dq[q] = (double)( (DOUT+(DIN-DOUT)*((double)(NX-i-procoffx[world_rank])+0.5)/(double)NX) * w[q] );
          #elif (defined DENSITY_YM && defined DENSITY_YP && !defined TWOPHASE)
          f[idx].dq[q] = (double)( (DOUT+(DIN-DOUT)*((double)(NY-j-procoffy[world_rank])+0.5)/(double)NY) * w[q] );
          #elif (defined DENSITY_ZM && defined DENSITY_ZP && !defined TWOPHASE)
          f[idx].dq[q] = (double)( (DOUT+(DIN-DOUT)*((double)(NZ-k-procoffz[world_rank])+0.5)/(double)NZ) * w[q] );

          #else
          f[idx].dq[q] = (double)( RHO0 * w[q] ); 
          #endif

          #ifdef THERMAL
          t[idx].dq[q] = (double)( TINIT * w[q] );
          #endif

          }

        #ifdef TWOPHASE
        } else if (lsol[idx] == 2) {

          for (q=0; q<NQ; q++) {
          f[idx].dq[q] = (double)( RHO2 * w[q] );

          #ifdef THERMAL
          t[idx].dq[q] = (double)( TINIT * w[q] );
          #endif

          }

        // Restore fluid in lsol
        lsol[idx] =  (int)1;
        #endif


        } else if (lsol[idx] == 0) {

        // do nothing ..

        #ifdef THERMAL
          for (q=0; q<NQ; q++) {
          t[idx].dq[q] = (double)( TWALL * w[q] );
          }
        #endif


        #ifdef THERMAL
	} else if (lsol[idx] == 3) {

          for (q=0; q<NQ; q++) {
          t[idx].dq[q] = (double)( TSUB * w[q] );
          }

        // Restore wall in lsol
        lsol[idx] =  (int)0;
        #endif

        } else {

        fprintf(stderr,"EE: Error in some values of geo.bin.txt file\n");
        exit(1);

        }
      }
    }
  }
}

int periodic_df (dfdq *f) {

  // Pay attention: the order Y->X->Z (opposite to the code order) 
  // is essential to avoid info leakeage!!

  MPI_Status status1;

  /* int MPI_Sendrecv(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
     int dest, int sendtag, void *recvbuf, int recvcount,
     MPI_Datatype recvtype, int source, int recvtag,
     MPI_Comm comm, MPI_Status *status) */

  MPI_Sendrecv(f+lzp*lxp*ly+lzp+1    , 1, mpi_dfplane_y, pryp, 3, \
               f+lzp+1               , 1, mpi_dfplane_y, prym, 3, mpi_comm_y, &status1);
  MPI_Sendrecv(f+lzp*lxp+lzp+1       , 1, mpi_dfplane_y, prym, 4, \
               f+lzp*lxp*(ly+1)+lzp+1, 1, mpi_dfplane_y, pryp, 4, mpi_comm_y, &status1);

  MPI_Sendrecv(f+lzp*lx+1    , 1, mpi_dfplane_x, prxp, 1, \
               f+1           , 1, mpi_dfplane_x, prxm, 1, mpi_comm_x, &status1);
  MPI_Sendrecv(f+lzp+1       , 1, mpi_dfplane_x, prxm, 2, \
               f+lzp*(lx+1)+1, 1, mpi_dfplane_x, prxp, 2, mpi_comm_x, &status1);

  MPI_Sendrecv(f+lz  , 1, mpi_dfplane_z, przp, 5, \
               f     , 1, mpi_dfplane_z, przm, 5, mpi_comm_z, &status1);
  MPI_Sendrecv(f+1   , 1, mpi_dfplane_z, przm, 6, \
               f+lz+1, 1, mpi_dfplane_z, przp, 6, mpi_comm_z, &status1);

}

#ifdef THERMAL
int micro_lbe_eq(int *lsol, dfdq *teq, dfdq *feq, macro *rho, macro *ux, macro *uy, macro *uz, macro *tem, macro *psix, macro *psiy, macro *psiz) {
#else
int micro_lbe_eq(int *lsol, dfdq *feq, macro *rho, macro *ux, macro *uy, macro *uz) {
#endif

  int idx=0, i=0, j=0, k=0, q=0;
  double C1=0.0;

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

        idx = IDXP(i,j,k);

        if (lsol[idx] != 0) {

          for (q=0; q<NQ; q++) {

          #ifdef LBMRT
          feq[idx].dq[q] = rho[idx]*EQ[q].eq[0] + rho[idx]* ( \
          ux[idx]*EQ[q].eq[1] + uy[idx]*EQ[q].eq[2] + uz[idx]*EQ[q].eq[3] ) + pow(rho[idx],2.)* (\
          ux[idx]*ux[idx]*EQ[q].eq[4] + uy[idx]*uy[idx]*EQ[q].eq[5] + uz[idx]*uz[idx]*EQ[q].eq[6] + \
          ux[idx]*uy[idx]*EQ[q].eq[7] + uy[idx]*uz[idx]*EQ[q].eq[8] + ux[idx]*uz[idx]*EQ[q].eq[9] ) ;
          #else

            #ifdef THERMAL

              #ifdef TWOPHASE

              // [ZhangTian2008] two-way coupling with d0=1-2T
              C1 = (double)ceil((double)q/NQ);

              feq[idx].dq[q] = rho[idx]*w[q] * ( ((1.-C1)+tem[idx]*(3.*C1-2))*(double)(1./CS2) + \
              (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
              (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
              (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
              #else

              feq[idx].dq[q] = rho[idx]*w[q] * (1. +\
              (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
              (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
              (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
              #endif

            #else

            feq[idx].dq[q] = rho[idx]*w[q] * (1. +\
            (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
            (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
            (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));
            #endif

          #endif

          }

        #ifdef THERMAL

        // We need the hydrodynamic velocity 
        // for two-way coupling and successive dumping
        ux[idx] += (-(double)TAU+0.5) * (double) (psix[idx]/rho[idx]);
        uy[idx] += (-(double)TAU+0.5) * (double) (psiy[idx]/rho[idx]);
        uz[idx] += (-(double)TAU+0.5) * (double) (psiz[idx]/rho[idx]);

          for (q=0; q<NQ; q++) {

          teq[idx].dq[q] = tem[idx]*w[q] * (1. + \
          (double)(1./CS2)*(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q]) + \
          (double)(1./(2.*CS2*CS2))*pow(ux[idx]*cx[q]+uy[idx]*cy[q]+uz[idx]*cz[q],2.) - \
          (double)(1./(2.*CS2))*(pow(ux[idx],2.)+pow(uy[idx],2.)+pow(uz[idx],2.)));

          }
	
        #endif

        }
      }
    }
  }
}

int micro_forcing(int *lsol, macro *ux, macro *uy, macro *uz, macro *rho, dfdq *bf) {

  int idx=0, i=0, j=0, k=0, q=0;

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {
        for (q=0; q<NQ; q++) {

        idx = IDXP(i,j,k);

          if (lsol[idx] != 0) {

          #ifdef GRAVITY
          bf[idx].dq[q] = w[q]*( \
          (((double)cx[q]-ux[idx])*(double)(1./CS2) + \
           ((double)cx[q]*ux[idx])*(double)(1./pow(CS2,2.))*(double)cx[q])* \
            rho[idx]*(double)DPDL*(double)dpdlv[0] + \
          (((double)cy[q]-uy[idx])*(double)(1./CS2) + \
           ((double)cy[q]*uy[idx])*(double)(1./pow(CS2,2.))*(double)cy[q])* \
            rho[idx]*(double)DPDL*(double)dpdlv[1] + \
          (((double)cz[q]-uz[idx])*(double)(1./CS2) + \
           ((double)cz[q]*uz[idx])*(double)(1./pow(CS2,2.))*(double)cz[q])* \
            rho[idx]*(double)DPDL*(double)dpdlv[2] );

          #else
          bf[idx].dq[q] = w[q]*( \
          (((double)cx[q]-ux[idx])*(double)(1./CS2) + \
           ((double)cx[q]*ux[idx])*(double)(1./pow(CS2,2.))*(double)cx[q])* \
            (double)DPDL*(double)dpdlv[0] + \
          (((double)cy[q]-uy[idx])*(double)(1./CS2) + \
           ((double)cy[q]*uy[idx])*(double)(1./pow(CS2,2.))*(double)cy[q])* \
            (double)DPDL*(double)dpdlv[1] + \
          (((double)cz[q]-uz[idx])*(double)(1./CS2) + \
           ((double)cz[q]*uz[idx])*(double)(1./pow(CS2,2.))*(double)cz[q])* \
            (double)DPDL*(double)dpdlv[2] );
          #endif

          }
        }
      }
    }
  }
}

int micro_lbe_collision(int *lsol, dfdq *f, dfdq *feq, dfdq *bf, I19 *MRT) {

  int i=0, j=0, k=0, idx=0; 
  int q1=0, q2=0;

  #ifdef LBMRT
  // (19xntot)
  dfdq *A, *C, *D, *E, *F;
  A=(dfdq *)calloc(lxp*lyp*lzp, sizeof(dfdq)); 
  C=(dfdq *)calloc(lxp*lyp*lzp, sizeof(dfdq)); 
  D=(dfdq *)calloc(lxp*lyp*lzp, sizeof(dfdq)); 
  E=(dfdq *)calloc(lxp*lyp*lzp, sizeof(dfdq)); 
  F=(dfdq *)calloc(lxp*lyp*lzp, sizeof(dfdq)); 

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        if (lsol[idx] != 0) {
          for (q1=0; q1<NQ; q1++) {
            for (q2=0; q2<NQ; q2++) {
  
            A[idx].dq[q1] += (double)MRT[q1].dq[q2] * bf[idx].dq[q2];
            D[idx].dq[q1] += (double)MRT[q1].dq[q2] * f[idx].dq[q2];

            }
          }
        }
      }
    }
  }

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        if (lsol[idx] != 0) {
          for (q1=0; q1<NQ; q1++) {
            for (q2=0; q2<NQ; q2++) {

            C[idx].dq[q1] += ((double)EYE[q1].dq[q2] - 0.5 * S[q1].dq[q2]) * A[idx].dq[q2];
            E[idx].dq[q1] += S[q1].dq[q2] * (D[idx].dq[q2] - feq[idx].dq[q2]);

            }
          }
        }
      }
    }
  }

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        if (lsol[idx] != 0) {
          for (q1=0; q1<NQ; q1++) {
            for (q2=0; q2<NQ; q2++) {

            F[idx].dq[q1] += iMRT[q1].dq[q2] * (E[idx].dq[q2]-C[idx].dq[q2]);

            }
          }
        }
      }
    }
  }


  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        if (lsol[idx] != 0) {
          for (q1=0; q1<NQ; q1++) {

          f[idx].dq[q1] = f[idx].dq[q1] - F[idx].dq[q1];

          }
        }
      }
    }
  }

  free(A);
  free(C);
  free(D);
  free(E);
  free(F);

  #else

  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        if (lsol[idx] != 0) {
          for (q1=0; q1<NQ; q1++) {

          f[idx].dq[q1] = (1.-(double)(1./TAU)) * f[idx].dq[q1] + \
                              (double)(1./TAU) * feq[idx].dq[q1] + \
                          (1.-(double)(1./(2.*TAU))) * bf[idx].dq[q1] ;

          }
        }
      }
    }
  }

  #endif

}

int micro_lbe_streaming(int *lsol, dfdq *f, dfdq *feq, int flag) {

  // Implemented with bounce-back (No scan required)
  int idx=0, i=0, j=0, k=0, q=0;
  int idxc=0, ic=0, jc=0, kc=0, qc=0;
 
  // 1) zero at lsol=0 (can I avoid it?) 
  for (j=0; j<lyp; j++) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idx=IDXP(i,j,k);

        for (q=0; q<NQ; q++) {

          // Fluid dist.fun
          if (flag==0) {

          feq[idx].dq[q] = f[idx].dq[q] * (double)lsol[idx];

          // Temperature dist.fun
          } else {

          feq[idx].dq[q] = f[idx].dq[q];

          }
        }
      }
    }
  }

  // 2) Stream and bounce on the basis of lsol=0|1
  for (j=1; j<=ly; j++) {
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

      idx=IDXP(i,j,k);

        if (lsol[idx] != 0) {
          for (q=1; q<NQ; q++) {
  
          ic=(int)i-cx[q];
          jc=(int)j-cy[q];
          kc=(int)k-cz[q];
          qc=(int)iq[q];
  
          idxc= IDXP(ic,jc,kc);
  
            // Fluid dist.fun
            if (flag==0) {

            f[idx].dq[q] = feq[idxc].dq[q] * (double)lsol[idx] + \
  	                 feq[idx].dq[qc] * (double)(1-lsol[idxc]);

            // Temperature dist.fun
            } else {

            f[idx].dq[q] = feq[idxc].dq[q] * (double)lsol[idx];

            }
          }
        }
      }
    }
  }
}

int micro_lbe_slip(int *lsol, dfdq *f) {

  int idxe=0, idxi=0, q=0, i=0, j=0, k=0;

  #if defined FREESLIP_XP
  //Check if processor is involved
  if ((int)prx == (int)NEX-1) {

    for (j=0; j<lyp; j++) {
      for (k=0; k<lzp; k++) {

      idxe=IDXP(lxp-2,j,k);
      idxi=IDXP(lxp-3,j,k);

        if (lsol[idxe]==1) {

          for (q=0; q<5; q++) {
          f[idxe].dq[qsx[q+5]] = f[idxi].dq[qsx[q]];

          }
        }
      }
    }
  }
  #endif

  #if defined FREESLIP_XM
  //Check if processor is involved
  if ((int)prx == (int)0) {

    for (j=0; j<lyp; j++) {
      for (k=0; k<lzp; k++) {

      idxe=IDXP(1,j,k);
      idxi=IDXP(2,j,k);

        if (lsol[idxe]==1) {
          for (q=0; q<5; q++) {
          f[idxe].dq[qsx[q]] = f[idxi].dq[qsx[q+5]];

          }
        }
      }
    }
  }
  #endif

  #if defined FREESLIP_YP
  //Check if processor is involved
  if ((int)pry == (int)NEY-1) {

    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idxe=IDXP(i,lyp-2,k);
      idxi=IDXP(i,lyp-3,k);

        if (lsol[idxe]==1) {
          for (q=0; q<5; q++) {
          f[idxe].dq[qsy[q+5]] = f[idxi].dq[qsy[q]];

          }
        }
      }
    }
  }
  #endif

  #if defined FREESLIP_YM
  //Check if processor is involved
  if ((int)pry == (int)0) {

    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      idxe=IDXP(i,1,k);
      idxi=IDXP(i,2,k);

        if (lsol[idxe]==1) {
          for (q=0; q<5; q++) {
          f[idxe].dq[qsy[q]] = f[idxi].dq[qsy[q+5]];

          }
        }
      }
    }
  }
  #endif

  #if defined FREESLIP_ZP
  //Check if processor is involved
  if ((int)prz == (int)NEZ-1) {

    for (i=0; i<lxp; i++) {
      for (j=0; j<lyp; j++) {

      idxe=IDXP(i,j,lzp-2);
      idxi=IDXP(i,j,lzp-3);

        if (lsol[idxe]==1) {
          for (q=0; q<5; q++) {
          f[idxe].dq[qsz[q+5]] = f[idxi].dq[qsz[q]];

          }
        }
      }
    }
  }
  #endif

  #if defined FREESLIP_ZM
  //Check if processor is involved
  if ((int)prz == (int)0) {

    for (i=0; i<lxp; i++) {
      for (j=0; j<lyp; j++) {

      idxe=IDXP(i,j,1);
      idxi=IDXP(i,j,2);

        if (lsol[idxe]==1) {
          for (q=0; q<5; q++) {
          f[idxe].dq[qsz[q]] = f[idxi].dq[qsz[q+5]];

          }
        }
      }
    }
  }
  #endif

}

#ifdef THERMAL
int micro_lbe_inout(int *lsol, dfdq *f, dfdq *t, macro *rho, macro *ux, macro *uy, macro *uz, macro *tem) {
#else
int micro_lbe_inout(int *lsol, dfdq *f, macro *rho, macro *ux, macro *uy, macro *uz) {
#endif

  int q=0, i=0, j=0, k=0, idx=0, idx1=0;
  int ckk=0, kbi=0, kai=0;
  double var=0.0;

  // Inlet
  #if (defined DENSITY_XM || defined DENSITY_YM || defined DENSITY_ZM || \
       defined VELOCITY_XM || defined VELOCTITY_YM || defined VELOCITY_ZM)

  #if (defined DENSITY_XM || defined VELOCITY_XM)
  //Check if processor is involved
  if ((int)prx == (int)0) {

    i = 1;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_YM || defined VELOCITY_YM)
  //Check if processor is involved
  if ((int)pry == (int)0) {

    j = 1;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_ZM || defined VELOCITY_ZM)
  //Check if processor is involved
  if ((int)prz == (int)0) {

    k = 1;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

  #endif

      idx=IDXP(i,j,k);
      var = 0.0;

      #if (defined DENSITY_XM || defined VELOCITY_XM)
      idx1 = IDXP(i-1,j,k);
      #elif (defined DENSITY_YM || defined VELOCITY_YM)
      idx1 = IDXP(i,j-1,k);
      #elif (defined DENSITY_ZM || defined VELOCITY_ZM)
      idx1 = IDXP(i,j,k-1);
      #endif

        if (lsol[idx]==1) {

          // Momentum correction
          for (q=0; q<NQ; q++) {

          #if (defined DENSITY_XM || defined VELOCITY_XM)
          ckk = cx[q];
          #elif (defined DENSITY_YM || defined VELOCITY_YM)
          ckk = cy[q];
          #elif (defined DENSITY_ZM || defined VELOCITY_ZM)
          ckk = cz[q];
          #endif

          kbi = (int)(1-(int)pow(ckk*ckk,0.5));
          kai = (int)(1-(int)floor(1.+(double)(ckk/2.)));
          var += (double)kbi*f[idx].dq[q] + 2.*(double)kai*f[idx].dq[q];

          }

        // Hydrodynamic velocity
        #ifdef DENSITY_XM
        ux[idx] = +1.0-(double)(var/DIN);
        #elif defined DENSITY_YM
        uy[idx] = +1.0-(double)(var/DIN);
        #elif defined DENSITY_ZM
        uz[idx] = +1.0-(double)(var/DIN);

        // Density from constant hydrodynamic velocity 
        #elif (defined VELOCITY_XM || defined VELOCITY_YM || defined VELOCITY_ZM)
        rho[idx] = (double)(var/(+1.0-VIN));
        #endif

          // Distribution functions correction [Zou He PoF 1997]
          for (q=0; q<5; q++) {

          #ifdef DENSITY_XM
          f[idx].dq[qsx[q]] = f[idx].dq[iq[qsx[q]]] + 2.*w[qsx[q]]/(double)CS2*DIN*ux[idx];
          #elif defined DENSITY_YM
          f[idx].dq[qsy[q]] = f[idx].dq[iq[qsy[q]]] + 2.*w[qsy[q]]/(double)CS2*DIN*uy[idx];
          #elif defined DENSITY_ZM
          f[idx].dq[qsz[q]] = f[idx].dq[iq[qsz[q]]] + 2.*w[qsz[q]]/(double)CS2*DIN*uz[idx];

          #elif defined VELOCITY_XM
          f[idx].dq[qsx[q]] = f[idx].dq[iq[qsx[q]]] + 2.*w[qsx[q]]/(double)CS2*rho[idx]*VIN;
          #elif defined VELOCITY_YM
          f[idx].dq[qsy[q]] = f[idx].dq[iq[qsy[q]]] + 2.*w[qsy[q]]/(double)CS2*rho[idx]*VIN;
          #elif defined VELOCITY_ZM
          f[idx].dq[qsz[q]] = f[idx].dq[iq[qsz[q]]] + 2.*w[qsz[q]]/(double)CS2*rho[idx]*VIN;

          #endif

          }

          // Update on neighbors sides (1)
          //for (q=0; q<NQ; q++) { 
          //f[idx1].dq[q] = f[idx].dq[q];
          //}
        }
      }
    }
  }

  #endif

  // Outlet
  #if (defined DENSITY_XP || defined DENSITY_YP || defined DENSITY_ZP || \
       defined VELOCITY_XP || defined VELOCTITY_YP || defined VELOCITY_ZP)

  #if (defined DENSITY_XP || defined VELOCITY_XP)
  //Check if processor is involved
  if ((int)prx == (int)NEX-1) {

    i = lx;
    for (j=1; j<=ly; j++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_YP || defined VELOCITY_YP)
  //Check if processor is involved
  if ((int)pry == (int)NEY-1) {

    j = ly;
    for (i=1; i<=lx; i++) {
      for (k=1; k<=lz; k++) {

  #elif (defined DENSITY_ZP || defined VELOCITY_ZP)
  //Check if processor is involved
  if ((int)prz == (int)NEZ-1) {

    k = lz;
    for (j=1; j<=ly; j++) {
      for (i=1; i<=lx; i++) {

  #endif

      idx=IDXP(i,j,k);
      var = 0.0;

      #if (defined DENSITY_XP || defined VELOCITY_XP)
      idx1 = IDXP(i+1,j,k);
      #elif (defined DENSITY_YP || defined VELOCITY_YP)
      idx1 = IDXP(i,j+1,k);
      #elif (defined DENSITY_ZP || defined VELOCITY_ZP)
      idx1 = IDXP(i,j,k+1);
      #endif

        if (lsol[idx]==1) {

          // Momentum correction
          for (q=0; q<NQ; q++) {

          #if (defined DENSITY_XP || defined VELOCITY_XP)
          ckk = cx[q];
          #elif (defined DENSITY_YP || defined VELOCITY_YP)
          ckk = cy[q];
          #elif (defined DENSITY_ZP || defined VELOCITY_ZP)
          ckk = cz[q];
          #endif

          kbi = (int)(1-(int)pow(ckk*ckk,0.5));
          kai = (int)(1-(int)floor(1.-(double)(ckk/2.)));
          var += (double)kbi*f[idx].dq[q] + 2.*(double)kai*f[idx].dq[q];

          }

        // Hydrodynamic velocity
        #ifdef DENSITY_XP
        ux[idx] = -1.0+(double)(var/DOUT);
        #elif defined DENSITY_YP
        uy[idx] = -1.0+(double)(var/DOUT);
        #elif defined DENSITY_ZP
        uz[idx] = -1.0+(double)(var/DOUT);

        // Density from constant hydrodynamic velocity 
        #elif (defined VELOCITY_XP || defined VELOCITY_YP || defined VELOCITY_ZP)
        rho[idx] = (double)(var/(+1.0+VOUT));
        #endif

          // Distribution functions correction [Zou He PoF 1997]
          for (q=0; q<5; q++) {

          #ifdef DENSITY_XP
          f[idx].dq[iq[qsx[q]]] = f[idx].dq[qsx[q]] - 2*w[qsx[q]]/(double)CS2*DOUT*ux[idx];
          #elif defined DENSITY_YP
          f[idx].dq[iq[qsy[q]]] = f[idx].dq[qsy[q]] - 2*w[qsy[q]]/(double)CS2*DOUT*uy[idx];
          #elif defined DENSITY_ZP
          f[idx].dq[iq[qsz[q]]] = f[idx].dq[qsz[q]] - 2*w[qsz[q]]/(double)CS2*DOUT*uz[idx];

          #elif defined VELOCITY_XP
          f[idx].dq[qsx[q]] = f[idx].dq[iq[qsx[q]]] + 2.*w[qsx[q]]/(double)CS2*rho[idx]*VOUT;
          #elif defined VELOCITY_YP
          f[idx].dq[qsy[q]] = f[idx].dq[iq[qsy[q]]] + 2.*w[qsy[q]]/(double)CS2*rho[idx]*VOUT;
          #elif defined VELOCITY_ZP
          f[idx].dq[qsz[q]] = f[idx].dq[iq[qsz[q]]] + 2.*w[qsz[q]]/(double)CS2*rho[idx]*VOUT;

          #endif

          }

          // Update on neighbors sides (1)
          //for (q=0; q<NQ; q++) { 
          //f[idx1].dq[q] = f[idx].dq[q];
	  //}
        }
      }
    }
  }

  #endif

}

int micro_lbe_chk(dfdq *f, int *lsol, char *flag) {

  int idx=0, i=0, j=0, k=0, q=0;
  FILE *fileout;
  char filename[128];

  sprintf(filename,"out/%s.chk.%d.dat",flag,world_rank);
  fileout = fopen(filename,"w");

  for (j=0; j<lyp; j++) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      q = 0;
      idx = IDXP(i,j,k);
      fprintf(fileout,"%d %d %d %g %d\n",i,j,k,f[idx].dq[q],lsol[idx]);

      }
    }
  }

  fclose(fileout);

}

