#include <sys/time.h>
#include <sys/resource.h>
#include <stdlib.h>
#include <stdio.h>

int setmemlimit() {

  struct rlimit memlimit;

  int  MEM = 8192;

  fprintf(stderr,"II: Setting FILE memory limit to: %05d \n",MEM);

  memlimit.rlim_cur = MEM;
  memlimit.rlim_max = MEM;
  setrlimit(RLIMIT_NOFILE, &memlimit);

}
