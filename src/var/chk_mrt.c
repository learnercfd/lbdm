#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <stddef.h>
#include "shared.h"

int chk_mrt_matrix(I19 *MRT, D19 *iMRT, I19 *EYE, D19 *S, D10 *EQ) {

  if (ROOT(world_rank))
  fprintf(stderr,"II: printing the inverted MRT [%dx%d] matrix and the others.\n",(int)NQ,(int)NQ);

  int i=0,j=0;
  
  FILE *fileprint;
  char filename[128];

  sprintf(filename,"out/mrt_matrix.dat");
  fileprint = fopen(filename,"w"); 
  fprintf(fileprint,"i j MRT iMRT EYE S EQ\n");

  for (j=0;j<NQ;j++) {
    for (i=0;i<NQ;i++) {

      if (j < 10) {
      fprintf(fileprint,"%d %d %d %g %d %g %g\n",i,j,(int)MRT[i].dq[j],(double)iMRT[i].dq[j], \
      EYE[i].dq[j],S[i].dq[j],EQ[i].eq[j]);
      } else {
      fprintf(fileprint,"%d %d %d %g %d %g %g\n",i,j,(int)MRT[i].dq[j],(double)iMRT[i].dq[j], \
      EYE[i].dq[j],S[i].dq[j],0.0);
      }
    }
  }

  fclose(fileprint);

}

