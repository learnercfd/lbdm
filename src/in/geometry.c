#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include <unistd.h>
#include "geometry.h"
#include "../var/shared.h"

int geometry_from_file(int *gsol) {

  FILE *filein;
  char filename[128];
  double solid=0.0;
  int count=0;
  
  // Must be in Z,X,Y order!!
  sprintf(filename,"in/%s",GEOM);

  if (access(filename,F_OK) != -1 ) {
    if (ROOT(world_rank))
    fprintf(stderr,"II: I found the %s file. Reading it.\n",filename);

  filein = fopen(filename,"r");
    while( fscanf(filein,"%lf\n",&solid)!=EOF && count<(NX*NY*NZ)) {
    gsol[count]=(int)solid;
    count++;
    }

  fclose(filein);

  } else {
  fprintf(stderr,"EE: I cannot find the %s file.\n",filename);
  exit(1);
  }

}


int geometry_mpi(int *gsol, int *lsol) {

  if (ROOT(world_rank))
  fprintf(stderr,"II: Start to decompose MPI domain.\n");
  
  int i=0, j=0, k=0;
  int idxg=0, idxl=0;
  int offx=0, offy=0, offz=0;
  
  offx = (int) procoffx[world_rank];
  offy = (int) procoffy[world_rank];
  offz = (int) procoffz[world_rank];

  for (j=0; j<NY; j++) {
    for (i=0; i<NX; i++) {
      for (k=0; k<NZ; k++) {
        if ( i>=offx && i<offx+lx && j>=offy && j<offy+ly && k>=offz && k<offz+lz ) { 

	// Single BELT +1 
        idxg = IDXG(i,j,k); 
        idxl = IDXP(i-offx+1,j-offy+1,k-offz+1);

          if ( i==offx && j==offy && k==offz ) 
          fprintf(stderr,"II: Mpi decomposition proc: %d\n",world_rank);

        lsol[idxl]=gsol[idxg];

        }
      }
    }
  }
}

int geometry_slip(int *lsol) {

  int idxe=0, idxi=0, i=0, j=0, k=0;

  #if (defined FREESLIP_XP || defined FREESLIP_XM)

  // Overwrite top boundary
  if (prx == (int)NEX-1) {
    for (j=0; j<lyp; j++) {
      for (k=0; k<lzp; k++) {

      //idxi=IDXP(lxp-2,j,k);
      idxe=IDXP(lxp-1,j,k);
      lsol[idxe] = 0;//lsol[idxi];

      }
    }
  }

  // Overwrite low boundary
  if (prx == 0) {
    for (j=0; j<lyp; j++) {
      for (k=0; k<lzp; k++) {

      //idxi=IDXP(1,j,k);
      idxe=IDXP(0,j,k);
      lsol[idxe] = 0;//lsol[idxi];

      }
    }
  }

  #endif

  #if (defined FREESLIP_YP || defined FREESLIP_YM)

  // Overwrite top boundary
  if (pry == (int)NEY-1) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      //idxi=IDXP(lxp-2,j,k);
      idxe=IDXP(i,lyp-1,k);
      lsol[idxe] = 0;//lsol[idxi];

      }
    }
  }

  // Overwrite low boundary
  if (pry == 0) {
    for (i=0; i<lxp; i++) {
      for (k=0; k<lzp; k++) {

      //idxi=IDXP(i,1,k);
      idxe=IDXP(i,0,k);
      lsol[idxe] = 0;//lsol[idxi];

      }
    }
  }

  #endif

  #if (defined FREESLIP_ZP || defined FREESLIP_ZM)

  // Overwrite top boundary
  if (prz == (int)NEZ-1) {
    for (j=0; j<lyp; j++) {
      for (i=0; i<lxp; i++) {

      //idxi=IDXP(i,j,lzp-2);
      idxe=IDXP(i,j,lzp-1);
      lsol[idxe] = 0;//lsol[idxi];

      }
    }
  }

  // Overwrite low boundary
  if (prz == 0) {
    for (j=0; j<lyp; j++) {
      for (i=0; i<lxp; i++) {

      //idxi=IDXP(i,j,1);
      idxe=IDXP(i,j,0);
      lsol[idxe] = 0;//lsol[idxi];

      }
    }
  }

  #endif

  // Boring 12-cases edges at zero,
  // I wait everyone to avoid overwriting
  MPI_Barrier(MPI_COMM_WORLD);

//  // 1st
//  #if (defined FREESLIP_XP && defined FREESLIP_YP)
//  if ( (prx==(int)NEX-1) && (pry==(int)NEY-1) ) {
//    for (k=0; k<lzp; k++) { 
//    idxe=IDXP(lxp-1,lyp-1,k);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 2nd
//  #if (defined FREESLIP_XP && defined FREESLIP_YM)
//  if ( (prx==(int)NEX-1) && (pry==0) ) {
//    for (k=0; k<lzp; k++) { 
//    idxe=IDXP(lxp-1,0,k);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 3rd
//  #if (defined FREESLIP_XM && defined FREESLIP_YP)
//  if ( (prx==0) && (pry==(int)NEY-1) ) {
//    for (k=0; k<lzp; k++) { 
//    idxe=IDXP(0,lyp-1,k);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 4th
//  #if (defined FREESLIP_XM && defined FREESLIP_YM)
//  if ( (prx==0) && (pry==0) ) {
//    for (k=0; k<lzp; k++) { 
//    idxe=IDXP(0,0,k);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 5th
//  #if (defined FREESLIP_XP && defined FREESLIP_ZP)
//  if ( (prx==(int)NEX-1) && (prz==(int)NEZ-1) ) {
//    for (j=0; j<lyp; j++) { 
//    idxe=IDXP(lxp-1,j,lzp-1);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 6th
//  #if (defined FREESLIP_XP && defined FREESLIP_ZM)
//  if ( (prx==(int)NEX-1) && (prz==0) ) {
//    for (j=0; j<lyp; j++) { 
//    idxe=IDXP(lxp-1,j,0);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 7th
//  #if (defined FREESLIP_XM && defined FREESLIP_ZP)
//  if ( (prx==0) && (prz==(int)NEZ-1) ) {
//    for (j=0; j<lyp; j++) { 
//    idxe=IDXP(0,j,lzp-1);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 8th
//  #if (defined FREESLIP_XM && defined FREESLIP_ZM)
//  if ( (prx==0) && (prz==0) ) {
//    for (j=0; j<lyp; j++) { 
//    idxe=IDXP(0,j,0);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 9th
//  #if (defined FREESLIP_YP && defined FREESLIP_ZP)
//  if ( (pry==(int)NEY-1) && (prz==(int)NEZ-1) ) {
//    for (i=0; i<lxp; i++) { 
//    idxe=IDXP(i,lyp-1,lzp-1);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 10th
//  #if (defined FREESLIP_YP && defined FREESLIP_ZM)
//  if ( (pry==(int)NEY-1) && (prz==0) ) {
//    for (i=0; i<lxp; i++) { 
//    idxe=IDXP(i,lyp-1,0);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 11th
//  #if (defined FREESLIP_YM && defined FREESLIP_ZP)
//  if ( (pry==0) && (prz==(int)NEZ-1) ) {
//    for (k=0; k<lzp; k++) { 
//    idxe=IDXP(i,0,lzp-1);
//    lsol[idxe]=0;
//    }
//  }
//  #endif
//
//  // 12th
//  #if (defined FREESLIP_YM && defined FREESLIP_ZM)
//  if ( (prx==0) && (pry==0) ) {
//    for (i=0; i<lxp; i++) { 
//    idxe=IDXP(i,0,0);
//    lsol[idxe]=0;
//    }
//  }
//  #endif

}
