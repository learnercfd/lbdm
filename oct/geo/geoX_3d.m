
 function [liq_sol_ijk]=geoX_3d(name,vA)


 pp=1;

nx=100;
ny=100;
nz=100;

%%% Complete medium (10/2016)
%A=load([ 'xtomo/scan-' name '/' name '.mat' ]);

%%% Small medium (12/2016)
load([ 'h5/' name '.h5' ]);
C=scan; clear scan;
printf([ '-> loaded ' name ' with direction vA: %i <-\n\n' ],vA),

A=C(1:nz,1:nx,1:ny);
clear C;

A=reshape(A,[nz nx ny]);
size(A),

%mirroring!!
B(:,:,1:ny)=[ A,flipdim(A,2);flipdim(A,1),flipdim(flipdim(A,1),2) ];
B(:,:,ny+1:2*ny)= [flipdim(A,3),flipdim(flipdim(A,3),2); ...
		flipdim(flipdim(A,3),1),flipdim(flipdim(flipdim(A,3),1),2) ];

A=B;
clear B;
printf('-> Mirroring done for final size: %i %i %i <-\n\n',size(A,1),size(A,2),size(A,2));

if (vA==2); A=permute(A,[2 3 1]); end;
if (vA==3); A=permute(A,[3 1 2]); end;
   
%% coeff
porosity=sum(sum(sum(A,3),2),1)/(nz*nx*ny*8),
%pause(),

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% indices
[isolid,jsolid,ksolid]=ind2sub(size(A),find(A==0));
Length=max(length(isolid));
liq_sol_ijk=zeros(Length,4);
liq_sol_ijk(1:length(isolid),1)=isolid;
liq_sol_ijk(1:length(isolid),2)=jsolid;
liq_sol_ijk(1:length(isolid),3)=ksolid;

liq_sol_ijk(1:4,4)=[2*nz,2*nx,2*ny,porosity]';
