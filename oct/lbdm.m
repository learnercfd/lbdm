
%%%% : this is a Lattice Boltmann Model 3D MRT D3Q19 single-phase : %%%

					%%%%%% DECLARE IF %%%%%%%%%%%%%

				 	name='8888'
					geoname='geoS_3d'
					
					% �� use of body force ??
					bf=1;

					% restart
					restart=0;

%%% looop sims
 
AfA=[1,0,0;
    10,0,0]; 

for fA=[1];
for vA=[1];

afA=AfA(fA,:);
dfA=6,dvA=6
rA=1.5,ncA=50,oA=1,


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% 1 %%%% SET-UP Model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if restart==0;

	% close all,
	clear -x name geoname bf restart afA AfA fA vA oA rA ncA dfA dvA;

tic

g_density=1.0;

%%% FORCING & INPUTS %%%%%%%

n=2; %resolution
df=dfA; %check

addpath('geo:in');

% Set a Reynolds ..
Re=0.1;
rfb_input=str2func([ 'rfb_input' ]);
dPdL=rfb_input(Re,n);

if (geoname=='geoS_3d');
spr=sprintf('%1.0f%1.0f%1.0f%1.0f',[n,oA,fA,vA]);
name=spr,
end;

%%%% TIME relaxation %%%%%%%

nueta=5/57; %0.0878;
tau=1.5;
Sxplus=1/tau;
Sxminus=(nueta/(1/Sxplus-0.5)+0.5)^-1;

C_ux0=0.3.*g_density;

%%%% SEVERAL GEOMETRIES %%%%

sprgeo=sprintf([ geoname ]);
geoN=str2func(sprgeo);

% Remember to put the right inputs

if (geoname=='geoX_3d');
[liq_sol_ijk]=geoN(name,vA); 

elseif (geoname=='geoC_3d');
[liq_sol_ijk]=geoN(n,dfA,dvA);

elseif (geoname=='geoS_3d');
[liq_sol_ijk]=geoN(n,rA,ncA,oA,vA,fA,name,80);

end;

%%% Build GEO %%%%%%%%%%%%%%

Nr=liq_sol_ijk(1,4); Mc=liq_sol_ijk(2,4); Ld=liq_sol_ijk(3,4);
porosity=liq_sol_ijk(4,4);

% porosity ... etc
geoname, name, porosity,

liq_sol_ijk=[liq_sol_ijk(:,1:3);zeros(1,3)];
solidLength=find(liq_sol_ijk(:,1)==0,1)-1;

Channel3D=ones(Nr,Mc,Ld);

for ijksolid=1:solidLength
isolid=liq_sol_ijk(ijksolid,1);
jsolid=liq_sol_ijk(ijksolid,2);
ksolid=liq_sol_ijk(ijksolid,3);
Channel3D(isolid,jsolid,ksolid)=0;
end

%%% memfree
clear liq_sol_ijk

%%% BBs and GEOs properties %%%

wW=sum(sum(Channel3D(:,1,:),1),3); 
wE=sum(sum(Channel3D(:,Mc,:),1),3); 
wmin=min(min(sum(Channel3D(:,:,:),1)));

ijk_io=zeros(wW,12);
[ijk_io(1:wW,1),ijk_io(1:wW,2),ijk_io(1:wW,3)]= ind2sub([Nr,1,Ld],find(Channel3D(:,1,:))); ijk_io(1:wW,2)=1;
[ijk_io(1:wE,4),ijk_io(1:wE,5),ijk_io(1:wE,6)]= ind2sub([Nr,1,Ld],find(Channel3D(:,Mc,:))); ijk_io(1:wE,5)=Mc;

zmin=1;%find(Channel3D(:,round(Mc/2),Ld/2),1);
zzP=Nr/2;
wall=0;

%%%% PROPERTIES %%%%%%%%%%

% sound speed
cs2=1/3; %?

% fluid mean density 
sizemp=sum(sum(sum(Channel3D,1),2),3);

omega=tau.^-1;
cP_visco=(tau-0.5)*cs2*g_density;
Lky_visco=cP_visco/g_density; % lattice kinematic viscosity


%%%% Pressure Gradient %%% �2D!

dch=Nr; % ???
ux_fin_max=dPdL.*(dch).^2.*(1/(8*cP_visco));
ux_av_in=2/3.*ux_fin_max; % Poiseuille
dP=dPdL*(Mc-1);

%  linear vel .. inizialization
u_in_ch=Re*Lky_visco/dch;
u_in_pm=dPdL*n^2/cP_visco;
u_out_ch=u_in_ch;
u_out_pm=u_in_pm;
w_in=0;
w_out=0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%











%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% 2 %%% DESCRIPTION of tHe Model %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% time relaxation PARAMETERS
%	  +   +   -   +   +    +    -
%	[Sx1,Sx2,Sx4,Sx9,Sx13.Sx10,Sx16]
Sx = ... 
	[Sxplus,Sxplus,Sxminus,Sxplus,Sxplus,Sxplus,Sxminus];


%%% analytic solution Poiseuille %%%%

% west & east
 yWch_profile=([-(dch/2):+(dch/2)-1]+0.5);
 u_profile=u_in_ch.*3/2.*(1-(yWch_profile/(dch/2)).^2);

% south
u_ioSg=repmat([g_density+dP/cs2.*(Mc-1:-1:0)./(Mc-1)]',1,Ld);

% west
u_ioWg=zeros(Nr,Ld);
u_ioEg=zeros(Nr,Ld);

REt=Re;


%%%% DIRECTIONS %%%%%%%

%	z		C_xzy =[x ,y ,z ]
%	^ y		       [+j,+k,-i]
%	|/
%	o--> x          

%	


% x & y & z components of velocities
N_c=19; % number of directions

% versors D3Q19
C_x=[0  1 -1  0  0  0  0  1 -1  1 -1  1 -1  1 -1  0  0  0  0 ]; 
C_y=[0  0  0  1 -1  0  0  1  1 -1 -1  0  0  0  0  1 -1  1 -1 ];
C_z=[0  0  0  0  0  1 -1  0  0  0  0  1  1 -1 -1  1  1 -1 -1 ];
C=[C_x;C_y;C_z];


%%% BOUNCE BACK SCHEME %%%%

% after collision the fluid elements densities f are sent back to the
% lattice node they come from with opposite direction
% indices opposite to 1:8 for fast inversion after bounce
%	  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19
ic_op = [ 1  3  2  5  4  7  6 11 10  9  8 15 14 13 12 19 18 17 16] ;
ic_sy = [ 1  2  3  4  5  7  6  8  9 10 11 14 15 12 13 18 19 16 17] ; 
%   i.e. 4 is opposite to 2 etc.

%%% PERIODIC BOUNDARY %%%%%

xi2=[Mc , 1:Mc , 1]; % Period Bound Cond
zi2=[Nr , 1:Nr , 1];
yi2=[Ld , 1:Ld , 1];


%%% INITIALIZATION %%%%%%%%%

% directional weights (density weights)
% and potential weights
w0=12/36 ; w1=2/36 ; w2=1/36;
W=[w0 w1 w1 w1 w1 w1 w1 w2 w2 w2 w2 w2 w2 w2 w2 w2 w2 w2 w2];

% M matrix
M  =	[ 1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1   1 ;
	-30 -11 -11 -11 -11 -11 -11   8   8   8   8   8   8   8   8   8   8   8   8 ;
	 12  -4  -4  -4  -4  -4  -4   1   1   1   1   1   1   1   1   1   1   1   1 ;
	  0   1  -1   0   0   0   0   1  -1   1  -1   1  -1   1  -1   0   0   0   0 ;
	  0  -4   4   0   0   0   0   1  -1   1  -1   1  -1   1  -1   0   0   0   0 ;
	  0   0   0   1  -1   0   0   1   1  -1  -1   0   0   0   0   1  -1   1  -1 ;
	  0   0   0  -4   4   0   0   1   1  -1  -1   0   0   0   0   1  -1   1  -1 ;
          0   0   0   0   0   1  -1   0   0   0   0   1   1  -1  -1   1   1  -1  -1 ;
	  0   0   0   0   0  -4   4   0   0   0   0   1   1  -1  -1   1   1  -1  -1 ;
	  0   2   2  -1  -1  -1  -1   1   1   1   1   1   1   1   1  -2  -2  -2  -2 ;
	  0  -4  -4   2   2   2   2   1   1   1   1   1   1   1   1  -2  -2  -2  -2 ;
	  0   0   0   1   1  -1  -1   1   1   1   1  -1  -1  -1  -1   0   0   0   0 ;
	  0   0   0  -2  -2   2   2   1   1   1   1  -1  -1  -1  -1   0   0   0   0 ;
	  0   0   0   0   0   0   0   1  -1  -1   1   0   0   0   0   0   0   0   0 ;
	  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   1  -1  -1   1 ;
	  0   0   0   0   0   0   0   0   0   0   0   1  -1  -1   1   0   0   0   0 ;
	  0   0   0   0   0   0   0   1  -1   1  -1  -1   1  -1   1   0   0   0   0 ;
	  0   0   0   0   0   0   0  -1  -1   1   1   0   0   0   0   1  -1   1  -1 ;
	  0   0   0   0   0   0   0   0   0   0   0   1   1  -1  -1  -1  -1   1   1 ];

% S matrix
S  = diag([0,Sx(1),Sx(2),0,Sx(3),0,Sx(3),0,Sx(3),Sx(4),Sx(5),Sx(4),Sx(5),Sx(6),Sx(6),Sx(6),Sx(7),Sx(7),Sx(7)]);

% EQ matrix
rd=g_density;
we=3;%0;
wej=-11/2;%-475/63;
wxx=-1/2;%0;

EQ = zeros(N_c,10);
EQ(1,1)=1; 
EQ(2,1)=-11; EQ(2,5)=19/rd; EQ(2,6)=19/rd; EQ(2,7)=19/rd;	
EQ(3,1)=we; EQ(3,5)=wej/rd; EQ(3,6)=wej/rd; EQ(3,7)=wej/rd; %% BGK
EQ(4,2)=1; EQ(6,3)=1; EQ(8,4)=1;
EQ(5,2)=-2/3; EQ(7,3)=-2/3; EQ(9,4)=-2/3;
EQ(10,5)=2/rd; EQ(10,6)=-1/(rd); EQ(10,7)=-1/(rd);
EQ(11,5)=2*wxx/rd; EQ(11,6)=-wxx/rd; EQ(11,7)=-wxx/rd;
EQ(12,6)=1/rd; EQ(12,7)=-1/rd;
EQ(13,6)=wxx/rd; EQ(13,7)=-wxx/rd; 
EQ(14,8)=1/rd; 
EQ(15,9)=1/rd;
EQ(16,10)=1/rd;

% c constants (sound speed related)
cs2=cs2; cs2x2=2*cs2; cs4x2=2*cs2.^2;
f1=1/cs2; f3=1/cs2x2; f2=1/cs4x2;
f1=3.; f2=4.5; f3=1.5; % coef. of the f equil.

% multi-reflection costant
kFpc=0; kb1=1+kFpc; kb0=0-kFpc; 
kubm2=0; kbm1=0; kubm1=0-kFpc;

% declarative statemets
f=zeros(Nr,Mc,Ld,N_c); % array of fluid density distribution
force=zeros(Nr,Mc,Ld,N_c);
feq=zeros(Nr,Mc,Ld,N_c); % f at equilibrium
rho=zeros(Nr,Mc,Ld); % macro-scopic density

% dimensionless velocities
ux=zeros(Nr,Mc,Ld);   uy=zeros(Nr,Mc,Ld); uz=zeros(Nr,Mc,Ld);


%%% EXTERNAL FORCES %%%%%%

%	1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
% cForce=[0  1 -1  0  0  0  0  1 -1  1 -1  1 -1  1 -1];

if bf==1;
fGradx(:,:,:,1) = dPdL.*Channel3D*afA(1); %sum(force.*cForce,2); % 0
fGradx(:,:,:,2) = dPdL.*Channel3D*afA(2);
fGradx(:,:,:,3) = dPdL.*Channel3D*afA(3);
else;
fGradx= [0,0,0];
end;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%












%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% 3 %%% While .. MAIN TIME EVOLUTION LOOP %%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



StopFlag=false; % i.e. logical(0)
Max_Iter=20000; % max allowed number of iteration
Check_Iter=1; Output_Every=50; % frequency check & output
Cur_Iter=0; % current iteration counter inizialization
toler=10^(-4); % tollerance to declare convegence
Cond_path=[]; % recording values of the convergence criterium
density_path=[]; % recording aver. density values for convergence
Condition2=toler; Iter_conv2=0; cond2=0;
Iter_Partial=Inf;
Max_Time=1%3600*12; %82800; %% 23h
saved=0;

for ic=1:N_c
f(:,:,:,ic)=1/N_c.*g_density.*Channel3D(:,:,:);
end

tic
end % end if restart=0

% restart from partial
if restart==1;
tic
toler=10^(-7);
StopFlag=false;
Condition2=toler; Iter_conv2=0; cond2=3;
Max_Iter=1000000;
end;



						while(~StopFlag)
    
Cur_Iter=Cur_Iter+1 % iteration counter update

% init velocities and density
ux=zeros(Nr,Mc,Ld); uy=zeros(Nr,Mc,Ld); uz=zeros(Nr,Mc,Ld);
rho=sum(f,4);


	%%% UPDATE VEL %%%%%%%%%%  

	for ic=2:1:N_c
	C_yic=C_y(ic); C_xic=C_x(ic); C_zic=C_z(ic);	
 
	if Cur_Iter > 0  % if start from a value
           
    	ux(:,:,:)=f(:,:,:,ic)*C_xic+ux(:,:,:);
	uy(:,:,:)=f(:,:,:,ic)*C_yic+uy(:,:,:);
	uz(:,:,:)=f(:,:,:,ic)*C_zic+uz(:,:,:);
	
    	else

    	for y=1:Ld;
	ux(:,:,y)=repmat(ux00(:,1,y),[1,Mc]).*Channel3D(:,:,y);
	uy(:,:,y)=repmat(uy00(:,1,y),[1,Mc]).*Channel3D(:,:,y);
	uz(:,:,y)=repmat(uz00(:,1,y),[1,Mc]).*Channel3D(:,:,y);
	end
    	
	end

	end

	% add force
	ux=ux+0.5.*fGradx(:,:,:,1);
	uy=uy+0.5.*fGradx(:,:,:,2);
	uz=uz+0.5.*fGradx(:,:,:,3);


%% UPDATE F EQ and RHO %%

% Equilibrium distribution
feq=zeros(Nr,Mc,Ld,N_c);	

	if Cur_Iter > 0 % if start from a value
	
	for ic=1:1:N_c
        
	feq(:,:,:,ic)=rho.*EQ(ic,1) +ux.*EQ(ic,2) +uy.*EQ(ic,3) ...
	+uz.*EQ(ic,4) +(ux.^2).*EQ(ic,5) ...
        +(uy.^2).*EQ(ic,6) +(uz.^2).*EQ(ic,7) ... 
	+(ux.*uy).*EQ(ic,8) +(uy.*uz).*EQ(ic,9) ... 
	+(ux.*uz).*EQ(ic,10) ;

	end

	else

	for y=1:Ld; for ic=1:1:N_c
	feq(:,:,y,ic)=repmat(feq00(:,1,y,ic),[1,Mc]).*Channel3D(:,:,y);
	f(:,:,y,ic)=repmat(f00(:,1,y,ic),[1,Mc]).*Channel3D(:,:,y);
	end; end

	end

rho=rho.*Channel3D;
% temp rho+1 to avoid NaN on boundaries
ux=ux./(rho+1-Channel3D); 
uy=uy./(rho+1-Channel3D); 
uz=uz./(rho+1-Channel3D);



%%% COLLISION %%%%% 

	% add body force
	for ic=1:N_c;	
        C_yic=C_y(ic); C_xic=C_x(ic); C_zic=C_z(ic);

	force(:,:,:,ic) = W(ic).* ...
	( ((C_xic-ux).*f1+(C_xic.*ux).*(2*f2*C_xic)).*fGradx(:,:,:,1) ...
 	+ ((C_yic-uy).*f1+(C_yic.*uy).*(2*f2*C_yic)).*fGradx(:,:,:,2) ...
	+ ((C_zic-uz).*f1+(C_zic.*uz).*(2*f2*C_zic)).*fGradx(:,:,:,3) ...
	);

	end;

f   = reshape(permute(f,[4,1,2,3]),N_c,Nr*Mc*Ld);
feq = reshape(permute(feq,[4,1,2,3]),N_c,Nr*Mc*Ld);

	f   = f - inv(M)*(S*(M*f-feq) - (eye(N_c)-0.5.*S)*(M*( ...
	reshape(permute(force,[4,1,2,3]),N_c,Nr*Mc*Ld)   )));	

f   = permute(reshape(f,N_c,Nr,Mc,Ld),[2,3,4,1]);
feq = permute(reshape(feq,N_c,Nr,Mc,Ld),[2,3,4,1]);
	

%%% STREAM %%%%%%%%

% Forward Propagation step & 
% Bounce Back (collision fluid with obstacles)
   
feq = f; % temp storage of f in feq

        for ic=2:1:N_c,
        ic2=ic_op(ic); % velocity opposite to ic for BB
        C_yic = C_y(ic); C_xic = C_x(ic); C_zic=C_z(ic);

	f(:,:,:,ic) = circshift(feq(:,:,:,ic),[-C_zic,C_xic,C_yic]).*(Channel3D) ... 
	+ kb1.*( feq(:,:,:,ic2) ...
	).*circshift((1-Channel3D(:,:,:)),[-C_zic,C_xic,C_yic]) ;

	end ; %  for ic direction

	% multireflection still NOT working!
%	C_yic2 = C_y(ic2); C_xic2= C_x(ic2); C_zic2=C_z(ic2);
%	- circshift((1-Channel3D(:,:,:)),[-C_zic,C_xic,C_yic])
%	+ kb0.*circshift(feq(:,:,:,ic2),[-C_zic2,C_xic2,C_yic2]) ...
%	+ kbm1.*circshift(feq(:,:,:,ic2),2.*[-C_zic2,C_xic2,C_yic2]) ...
%	+ kubm2.*circshift(feq(:,:,:,ic),[-C_zic2,C_xic2,C_yic2]) ...
%	+ kubm1.*feq(:,:,:,ic)...


%%% BBs %%%

%f=zouhebbss_3d(ijk_io,u_ioWg,u_ioEg,u_ioSl,u_ioSg,f,g_density,l_density, Cur_Iter,dP,bf,Iter_Partial);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % ends of Forward Propagation step &  Bounce Back Sections

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%












%%%%%%%%%% CONVERGENCE ? %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Convergence check on velocity values
    
if (mod(Cur_Iter,Check_Iter)==0) ; % every 'Check_Iter' iterations

	% averaging velocity (in the wet area)
	rho=sum(f,4); % density
	ux=((rho+1-Channel3D).*ux+0.0.*fGradx(:,:,:,1))./(rho+1-Channel3D); %-0.5.*fGradx
	uy=((rho+1-Channel3D).*uy+0.0.*fGradx(:,:,:,2))./(rho+1-Channel3D);
	uz=((rho+1-Channel3D).*uz+0.0.*fGradx(:,:,:,3))./(rho+1-Channel3D);

	Af=sum(sum(Channel3D,2),3); 
	AfP=sum(sum(Channel3D(zzP+1:Nr-wall,:,:),2),3); 
	AfC=sum(sum(Channel3D(zmin:zzP,:,:),2),3);

	vrho0=(sum(sum(rho(:,:,:),2),3)./Af);	
	vrho0P=(sum(sum(rho(zzP+1:Nr-wall,:),2),3)./AfP);
	vrho0C=(sum(sum(rho(zmin:zzP,:),2),3)./AfC);

	vrho0(isnan(vrho0))=0; rho0=mean(vrho0);
	vrho0P(isnan(vrho0P))=0; rho0P=mean(vrho0P);
	vrho0C(isnan(vrho0C))=0; rho0C=mean(vrho0C);

	ux_da=(sum(sum(ux.*Channel3D,2),3))./Af;
	ux_daC=sum(sum(ux(zmin:zzP,:,:).*Channel3D(zmin:zzP,:,:),2),3)./AfC;
	ux_daP=sum(sum(ux(zzP+1:Nr-wall,:,:).*Channel3D(zzP+1:Nr-wall,:,:),2),3)./AfP;

	ux_da(isnan(ux_da))=0; av_vel=mean(ux_da);
	ux_daC(isnan(ux_daC))=0; av_vel_channel=mean(ux_daC);
	ux_daP(isnan(ux_daP))=0; av_vel_porous=mean(ux_daP);

	k_darcy=av_vel_porous*cP_visco/dPdL;
        
	REtp0 = abs(av_vel)*(df)/cP_visco*rho0;
	REtpC = abs(av_vel_channel)*(dch)/cP_visco*rho0C; 
	REtpP = abs(av_vel_porous)*sqrt(abs(k_darcy))/cP_visco*rho0P; 

	REtp=REtp0;
	Condition=abs( abs(REt/REtp )-1); % should --> 0        
	REt=REtp; % time t & t+1 
 

	%%% TOLERANCE BREAK %%%

        if (Cur_Iter > Max_Iter | isnan(REt) | (size(Condition2,2)>2) )
            StopFlag=true;
            display( 'Stop iteration: iteration exeeding the max allowed or NaN value of Re or ... Yuppi converged!' )
            display( ['Current iteration: ',num2str(Cur_Iter),...
                ' Max Number of iter: ',num2str(Max_Iter),...
		' Reynolds numberrrr: ',num2str(REt) ] )
            break % Terminate execution of WHILE ..
        end
	
	if (Condition < toler) ;
	if    (Cur_Iter == Iter_conv2+Check_Iter) ;	    
	      cond2=cond2+1,
	else; cond2=1; end;
	Condition2(cond2)=Condition;
	Iter_conv2=Cur_Iter;
	end

        time=toc;

end


if ( (time > Max_Time) && (saved==0) );
printf('\n Max Time reached .. I m sorry \n\n');
spr=sprintf([ name '_%i'], vA);
save( [ 'out/' spr '.part.mat' ],'ux','uy','uz','Channel3D','rho','convergence','Condition2','name')
StopFlag=true;
saved=1,
break
end

%%% outputs every iteration output

    if (mod(Cur_Iter,Output_Every)==0) ;

	convergence(Cur_Iter/Output_Every)=Condition,
	vec_Iter=[Output_Every:Output_Every:Cur_Iter];
		
    end % every

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
end %  End main time Evolution Loop

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




% TIME

if restart==1;
toc;
TOC=TOC+toc;
else
toc; TOC=toc
end;

% saving
if (saved==0);
spr=sprintf([ name '_%i.mat'],vA);
save( [ 'out/' spr ],'ux','uy','uz','Channel3D','rho','convergence','Condition2','name')
saved=1,
end;

end; end;

