function [Phi]=phicalc_3d(rho,Nr,Mc,Ld,C_x,C_y,C_z,N_c,id,Channel3D,g_density,l_density,z_density,Geff);

% at present, work with body force only

inhib=0.00;
Phi0=eos(z_density,rho,Geff);

if inhib~=0;

Phig=eos(z_density,g_density,Geff);
Phil=eos(z_density,l_density,Geff);

u1=4/45*1/3; u2=1/21*1/3; u3=2/105*1/3; u4=5/504*1/3; 
u5=1/315*1/3; u6=1/630*1/3; u8=1/5040*1/3;
Cd1=[C_z(1),-C_x(1),-C_y(1)];
Cd3=[C_z(3),-C_x(3),-C_y(3)];
Cd5=[C_z(5),-C_x(5),-C_y(5)];

surpl=(Phi0-Phig);
surpl1=(Phi0-Phil);

Phim=zeros(Nr,Mc,Ld);
Phiwei=zeros(Nr,Mc,Ld);

% not yet ready for holes on bottom
% w2DN=[ones(2,Mc,Ld);zeros(Nr-2,Mc,Ld)];
% w2DS=[zeros(Nr-2,Mc,Ld);ones(2,Mc,Ld)];

w2D=ones(Nr,Mc,Ld);

for ic=[1:6]
C_xic=C_x(ic); C_yic=C_y(ic); C_zic=C_z(ic);
C_xid2=C_x(id(ic+2)); C_xid4=C_x(id(ic+4));
C_yid2=C_y(id(ic+2)); C_yid4=C_y(id(ic+4)); 
C_zid2=C_z(id(ic+2)); C_zid4=C_z(id(ic+4));
Phim=Phim+ circshift(Phi0.*Channel3D,[C_zic,-C_xic,-C_yic]).*u1+ circshift(Phi0.*Channel3D,2.*[C_zic,-C_xic,-C_yic]).*u4...
+ circshift(Phi0.*Channel3D,2.*[C_zic,-C_xic,-C_yic]+[C_zid2,-C_xid2,-C_yid2]).*u5 + circshift(Phi0.*Channel3D,2.*[C_zic,-C_xic,-C_yic]-[C_zid2,-C_xid2,-C_yid2]).*u5...
+ circshift(Phi0.*Channel3D,2.*[C_zic,-C_xic,-C_yic]+[C_zid4,-C_xid4,-C_yid4]).*u5 + circshift(Phi0.*Channel3D,2.*[C_zic,-C_xic,-C_yic]-[C_zid4,-C_xid4,-C_yid4]).*u5...
+ circshift(Phi0.*Channel3D,[C_zic,-C_xic,-C_yic]+[C_zid2,C_xid2,C_yid2]).*u2 + circshift(Phi0.*Channel3D,[C_zic,-C_xic,-C_yic]-[C_zid2,C_xid2,C_yid2]).*u2...
+ circshift(Phi0.*Channel3D,2.*[C_zic,-C_xic,-C_yic]+2.*[C_zid2,C_xid2,C_yid2]).*u8 + circshift(Phi0.*Channel3D,2.*[C_zic,-C_xic,-C_yic]-2.*[C_zid2,C_xid2,C_yid2]).*u8 ;
Phiwei=Phiwei+ circshift(Channel3D,[C_zic,-C_xic,-C_yic]).*u1+circshift(Channel3D,2.*[C_zic,-C_xic,-C_yic]).*u4...
+ circshift(Channel3D,2.*[C_zic,-C_xic,-C_yic]+[C_zid2,-C_xid2,-C_yid2]).*u5 + circshift(Channel3D,2.*[C_zic,-C_xic,-C_yic]-[C_zid2,-C_xid2,-C_yid2]).*u5...
+ circshift(Channel3D,2.*[C_zic,-C_xic,-C_yic]+[C_zid4,-C_xid4,-C_yid4]).*u5 + circshift(Channel3D,2.*[C_zic,-C_xic,-C_yic]-[C_zid4,-C_xid4,-C_yid4]).*u5...
+ circshift(Channel3D,[C_zic,-C_xic,-C_yic]+[C_zid2,C_xid2,C_yid2]).*u2 + circshift(Channel3D,[C_zic,-C_xic,-C_yic]-[C_zid2,C_xid2,C_yid2]).*u2...
+ circshift(Channel3D,2.*[C_zic,-C_xic,-C_yic]+2.*[C_zid2,C_xid2,C_yid2]).*u8 + circshift(Channel3D,2.*[C_zic,-C_xic,-C_yic]-2.*[C_zid2,C_xid2,C_yid2]).*u8 ;
end

for ic=[7:14]
C_xic=C_x(ic); C_yic=C_y(ic); C_zic=C_z(ic); 	
Phim=Phim+ circshift(Phi0.*Channel3D,[C_zic,-C_xic,-C_yic]).*u3+ circshift(Phi0.*Channel3D,[C_zic,-C_xic,-C_yic].*(1+Cd1)).*u6+ circshift(Phi0.*Channel3D,[C_zic,-C_xic,-C_yic].*(1+Cd3)).*u6+ circshift(Phi0.*Channel3D,[C_zic,-C_xic,-C_yic].*(1+Cd5)).*u6;
Phiwei=Phiwei+ circshift(Channel3D,[C_zic,-C_xic,-C_yic]).*u3+ circshift(Channel3D,[C_zic,-C_xic,-C_yic].*(1+Cd1)).*u6+ circshift(Channel3D,[C_zic,-C_xic,-C_yic].*(1+Cd3)).*u6+ circshift(Channel3D,[C_zic,-C_xic,-C_yic].*(1+Cd5)).*u6;
end;

Phi=Phig+surpl.*heaviside(Phim./Phiwei-inhib.*Phil);
Phi=Phi.*w2D;

else;

Phi=Phi0;

end;

