%%% EOS %%%

function Phi=eos(z_density,rho,Geff);

 Phi=sqrt(z_density).*(1-exp(-rho./z_density));

 return,

cs2=1/3;

%%% CS %%%

c0=1/3;

a=1;
b=4;
R=1;
T=0.7;

kcs=(1+(b.*rho./4)+(b.*rho./4).^2-(b.*rho./4).^3)./((1-(b.*rho./4)).^3);

P=rho.*(R*T).*kcs-a.*rho.^2;

%%% PR %%%

w=0.344;
a=2/49;
b=2/21;
R=1;
T=0.058;

Tc=(0.0778*R/b)/(0.45724*R^2/a);

pc=0.0778*R*Tc/b;
Tr=T/Tc;

alpha=(1+(0.37464+1.54226*w-0.26992*w^2)*(1-sqrt(T/Tc)))^2;

P=(rho.*(R*T))./(1-b.*rho)-((a*alpha).*rho.^2)./(1+(2*b).*rho-(b.*rho).^2);

%%%%%%%%

Phi_NAME=sqrt((P-rho.*cs2)./(Geff.*cs2/2));




